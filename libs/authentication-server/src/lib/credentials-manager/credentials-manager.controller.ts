import { Body, Controller, UseInterceptors, UsePipes } from '@nestjs/common'
import { MessagePattern } from '@nestjs/microservices'
import { CredentialsManagerService } from './credentials-manager.service'
import {
  QualifiedValidationWithExceptionPipe,
  ResponseTransformerInterceptor
} from '@doesrobbiedream/nest-core'
import { AuthenticationServerApi } from '../declarations/authentication-server.api'
import GetCredentialDTO = AuthenticationServerApi.DTOs.GetCredentialDTO
import UserResponse = AuthenticationServerApi.Responses.UserResponse
import SignCredentialsDTO = AuthenticationServerApi.DTOs.SignCredentialsDTO
import UserCredentials = AuthenticationServerApi.Responses.UserCredentials
import RefreshTokenDTO = AuthenticationServerApi.DTOs.RefreshTokenDTO

@Controller()
export class CredentialsManagerController {
  constructor(protected service: CredentialsManagerService) {}

  @MessagePattern('authentication.credentials-manager.credentials:get')
  @UsePipes(QualifiedValidationWithExceptionPipe)
  @UseInterceptors(new ResponseTransformerInterceptor(UserResponse))
  validateUser(@Body() credentials: GetCredentialDTO) {
    return this.service.validateUserCredentials(credentials)
  }

  @MessagePattern('authentication.credentials-manager.credentials:sign')
  @UsePipes(QualifiedValidationWithExceptionPipe)
  @UseInterceptors(new ResponseTransformerInterceptor(UserCredentials))
  signUserJWT(@Body() signature: SignCredentialsDTO) {
    return this.service.signUserCredentials(signature).then((jwt) => ({ jwt }))
  }

  @MessagePattern('authentication.credentials-manager.credentials:refresh')
  @UsePipes(QualifiedValidationWithExceptionPipe)
  @UseInterceptors(new ResponseTransformerInterceptor(UserCredentials))
  refreshUserJWT(@Body() payload: RefreshTokenDTO) {
    return this.service.refreshUserJWT(payload).then((jwt) => ({ jwt }))
  }
}
