import {
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  OnInit,
  Output,
  Renderer2
} from '@angular/core'
import { ImageFileParserService } from './image-file-parser.service'
import { from } from 'rxjs'
import { map, mergeMap, take } from 'rxjs/operators'

@Directive({
  selector: 'button[drdFileUploadButton], button[drd-file-upload-button]'
})
export class FileUploadButtonDirective implements OnInit {
  @Output() filesSelected: EventEmitter<
    { file: File; data: string | ArrayBuffer }[]
  > = new EventEmitter<{ file: File; data: string | ArrayBuffer }[]>()
  protected $input!: HTMLInputElement

  constructor(
    protected el: ElementRef,
    protected renderer: Renderer2,
    protected fileReader: ImageFileParserService
  ) {}

  @HostListener('click')
  onButtonClick() {
    this.$input.click()
  }

  ngOnInit() {
    this.renderInput()
  }

  protected renderInput() {
    this.$input = this.renderer.createElement('input') as HTMLInputElement
    this.$input.type = 'file'
    this.$input.multiple = true
    this.renderer.listen(this.$input, 'change', () => this.handleFiles())
  }

  protected handleFiles() {
    let results: { file: File; data: string | ArrayBuffer }[] = []
    from(this.$input.files || [])
      .pipe(
        mergeMap((f) => this.fileReader.readImage(f).pipe(map((data) => ({ file: f, data })))),
        take((this.$input.files || []).length)
      )
      .subscribe({
        next: (result) => (results = [...results, result]),
        complete: () => this.filesSelected.emit(results)
      })
  }
}
