module.exports = {
  projects: [
    '<rootDir>/libs/ng-core',
    '<rootDir>/libs/themify',
    '<rootDir>/libs/ts-utils',
    '<rootDir>/libs/nest-core',
    '<rootDir>/apps/nest-sandbox',
    '<rootDir>/libs/orchestrator-commons',
    '<rootDir>/apps/nest-ms-sandbox',
    '<rootDir>/libs/authentication-commons',
    '<rootDir>/libs/authentication-server',
    '<rootDir>/libs/authentication-client',
    '<rootDir>/libs/orchestrator-client',
    '<rootDir>/libs/orchestrator-server',
    '<rootDir>/libs/notification-server',
    '<rootDir>/libs/migrations',
    '<rootDir>/libs/console'
  ]
}
