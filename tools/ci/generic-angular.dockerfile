######################################################################
FROM node:12.13-alpine as prod-dependencies
######################################################################
WORKDIR /app
COPY package*.json yarn.lock yarn-* ./
RUN npm ci --production --ignore-scripts
######################################################################
FROM node:12.13-alpine as dev-dependencies
######################################################################
WORKDIR /app
COPY package*.json yarn.lock yarn-* ./
RUN npm ci --ignore-scripts
######################################################################
FROM node:12.13-alpine as builder
######################################################################
WORKDIR /app
ARG APP
ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV
COPY --from=dev-dependencies /app /app
COPY apps/$APP apps/$APP
COPY libs libs
COPY angular.json nx.json tsconfig.base.json ./
ENV NODE_ENV production
RUN $(npm bin)/nx run $APP:build --prod --configuration staging
######################################################################
FROM nginx:1.16.0-alpine as final
######################################################################
WORKDIR /usr/share/nginx/html
ARG APP
COPY --from=prod-dependencies /app  .
COPY --from=builder /app/dist/apps/$APP .

ENTRYPOINT ["nginx", "-g", "daemon off;"]
