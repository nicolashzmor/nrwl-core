import { IsEmail, IsNotEmpty, IsString, Matches } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'
import { Authentication } from '@doesrobbiedream/authentication-commons'

export namespace AuthenticationClientAPI {
  export namespace DTO {
    export class SignUpDTO {
      @IsNotEmpty()
      @IsString()
      @ApiProperty({ name: 'first_name' })
      first_name!: string
      @IsNotEmpty()
      @IsString()
      @ApiProperty({ name: 'last_name' })
      last_name!: string
      @IsNotEmpty()
      @IsEmail()
      @ApiProperty({ name: 'email' })
      email!: string
      @IsNotEmpty()
      @IsString()
      @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.!@#$%^&*])(?=.{8,})/)
      @ApiProperty({ name: 'password' })
      password!: string
    }

    export class SignInDTO {
      @IsNotEmpty()
      @IsString()
      @ApiProperty()
      username!: string
      @IsNotEmpty()
      @IsString()
      @ApiProperty()
      password!: string
    }

    export class RefreshCredentialsDTO {
      @IsNotEmpty()
      @IsString()
      @ApiProperty()
      refresh_token!: string
    }

    export class ResendVerificationLinkDTO {
      @ApiProperty()
      @IsNotEmpty()
      @IsEmail()
      email!: string
    }

    export class ResetPasswordRequestDTO {
      @ApiProperty()
      @IsNotEmpty()
      @IsEmail()
      email!: string
    }

    export class SetNewPasswordDTO {
      @IsNotEmpty()
      @IsString()
      @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.!@#$%^&*])(?=.{8,})/)
      @ApiProperty()
      password!: string
    }
  }
  export namespace Response {
    import Credentials = Authentication.Credentials

    export class CredentialsResponse implements Credentials {
      @ApiProperty({ name: 'jwt' })
      jwt!: string
    }
  }
}
