export interface ThemifyConfigurationInterface {
  setupCssVars?: boolean
  colors: {
    primary: string
    accent: string
    [key: string]: any
  }
  color_values?: {
    [key: string]: string
  }
}
