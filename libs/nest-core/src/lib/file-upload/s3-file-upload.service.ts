import { Inject, Injectable, Logger } from '@nestjs/common'
import Multer from 'multer'
import MulterS3 from 'multer-s3'
import _reduce from 'lodash/reduce'
import { FileUploadModuleValues } from './file-upload-module.values'
import { S3 } from 'aws-sdk'
import INSTANCE_OPTIONS = FileUploadModuleValues.INSTANCE_OPTIONS
import ModuleOptionsInterface = FileUploadModuleValues.ModuleOptionsInterface
import MulterS3BucketOptions = FileUploadModuleValues.MulterS3BucketOptions

@Injectable()
export class S3FileUploadService {
  protected options: ModuleOptionsInterface
  protected S3Connection!: S3
  protected uploaderList!: { [key: string]: Multer.Multer }

  constructor(@Inject(INSTANCE_OPTIONS) options: ModuleOptionsInterface) {
    this.options = options
    this.setup()
  }

  public uploader(bucket: string): Multer.Multer | null {
    if (!this.haveOptionsSetUp()) {
      return null
    }
    const uploader: any = this.uploaderList[bucket]
    if (uploader['DISABLED']) {
      throw Error('The requested bucket is not available')
    }
    return uploader
  }

  registerBucket(options: MulterS3BucketOptions): Multer.Multer | null {
    if (!this.haveOptionsSetUp()) {
      return null
    }
    const conn = options.s3 || this.S3Connection
    conn.listBuckets((err, data) => {
      if (err) {
        Logger.error('[FileUploadModule] No response on Buckets List')
        console.log(err)
        return
      }
      const exists = data?.Buckets?.find((bucket) => bucket.Name === options.bucket) || null
      if (!exists) {
        conn.createBucket({ ACL: options.acl, Bucket: options.bucket }, (e) => {
          if (e) {
            Logger.log('[FileUploadModule] Registering S3 Bucket | Failed')
            Logger.error(e)
            ;(this.uploaderList[options.bucket] as any)['DISABLED'] = true
          }
        })
      }
    })

    return Multer({ storage: MulterS3({ ...{ s3: this.S3Connection }, ...options }) })
  }

  protected setup() {
    if (!this.haveOptionsSetUp()) {
      return
    }
    this.S3Connection = new S3(this.options.S3Options)
    this.uploaderList = _reduce(
      this.options.buckets,
      (up, opt: MulterS3BucketOptions) => ({
        ...up,
        [opt.bucket]: this.registerBucket(opt)
      }),
      {}
    )
  }

  protected haveOptionsSetUp(log: boolean = true) {
    if (log && !this.options) {
      Logger.error(
        "S3 Service cannot found its' configuration. Please provide a configuration by calling FileUploadModule.forRoot"
      )
    }
    return !!this.options
  }
}
