## @doesrobbiedream/authentication-commons [1.3.2](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-commons@1.3.1...@doesrobbiedream/authentication-commons@1.3.2) (2021-07-29)





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.1.2

## @doesrobbiedream/authentication-commons [1.3.1](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-commons@1.3.0...@doesrobbiedream/authentication-commons@1.3.1) (2021-07-29)





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.1.1

# @doesrobbiedream/authentication-commons [1.3.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-commons@1.2.0...@doesrobbiedream/authentication-commons@1.3.0) (2021-06-29)


### Features

* **authentication-commons:** add expired-credential exception ([ef997d3](https://gitlab.com/doesrobbiedream/nrwl-core/commit/ef997d35cf5bf4234676366835064a0b343e0380))

# @doesrobbiedream/authentication-commons [1.2.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-commons@1.1.0...@doesrobbiedream/authentication-commons@1.2.0) (2021-06-29)


### Features

* **authentication-commons:** add refresh_token to Signature ([002b0fd](https://gitlab.com/doesrobbiedream/nrwl-core/commit/002b0fd13fc87e34ef40033b86f57470d20100be))

# @doesrobbiedream/authentication-commons [1.1.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/authentication-commons@1.0.0...@doesrobbiedream/authentication-commons@1.1.0) (2021-06-28)


### Features

* **authentication-client,authentication-commons,authentication-server,nest-core:** m ([f6a6099](https://gitlab.com/doesrobbiedream/nrwl-core/commit/f6a609934086444d1d8de836ce6db01aa7215eac))





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.1.0

# @doesrobbiedream/authentication-commons 1.0.0 (2021-06-17)


### Features

* **authentication-server:** implement controller tests and define module API ([2bb190b](https://gitlab.com/doesrobbiedream/nrwl-core/commit/2bb190b7250a20135b35274c2798d5d551deaa92))
* **authentication-server:** implement controller tests and define module API ([580f175](https://gitlab.com/doesrobbiedream/nrwl-core/commit/580f1751db7b68136a3ae5c274b40d61f05da431))
* **master:** automated library publishing ([b85d932](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b85d93299a57fad10f0de27c35b27e547f93c657))





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.0.0
