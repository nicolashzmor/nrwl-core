import { Pipe, PipeTransform } from '@angular/core'
import { ImageFileParserService } from './image-file-parser.service'
import { Observable } from 'rxjs'

@Pipe({
  name: 'imagePreview'
})
export class ImagePreviewPipe implements PipeTransform {
  constructor(protected fileReader: ImageFileParserService) {}

  transform(file: File): Observable<string | ArrayBuffer> {
    return (file && this.fileReader.readImage(file)) || null
  }
}
