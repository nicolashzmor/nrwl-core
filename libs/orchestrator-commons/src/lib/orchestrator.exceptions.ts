import { ExceptionHandlingDictionaries, RegisteredException } from '@doesrobbiedream/nest-core'

export namespace OrchestratorExceptions {
  import ErrorMessageDictionary = ExceptionHandlingDictionaries.ErrorMessageDictionary
  export const dictionary: ErrorMessageDictionary = {
    ORCHESTRATION_FAILED: {
      status: 500,
      message: 'Something went wrong with the process. Please, contact support.',
      exception: 'INTERNAL_SERVER_ERROR'
    }
  }

  export class OrchestrationFailedException extends RegisteredException {
    exception = 'ORCHESTRATION_FAILED'

    constructor(public data?: Record<string, any>) {
      super()
    }
  }
}
