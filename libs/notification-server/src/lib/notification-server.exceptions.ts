import { RegisteredException } from '@doesrobbiedream/nest-core'

export namespace NotificationServerExceptions {
  export class TransformerNotFoundException extends RegisteredException {
    exception = 'TRANSFORMER_NOT_FOUND'

    constructor(public data?: Record<string, any>) {
      super()
    }
  }

  export class EmailNotificationFailedException extends RegisteredException {
    exception = 'EMAIL_NOTIFICATION_FAILED'

    constructor(public data?: Record<string, any>, public error?: any) {
      super()
    }
  }
}
