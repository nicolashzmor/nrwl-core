import { InjectionToken } from '@angular/core'

export const CONFIG_TOKEN = new InjectionToken('THEMIFY_CONFIGURATION_TOKEN')
