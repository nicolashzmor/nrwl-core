## @doesrobbiedream/orchestrator-client [1.0.3](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/orchestrator-client@1.0.2...@doesrobbiedream/orchestrator-client@1.0.3) (2021-07-29)





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.1.2
* **@doesrobbiedream/orchestrator-commons:** upgraded to 1.0.3

## @doesrobbiedream/orchestrator-client [1.0.2](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/orchestrator-client@1.0.1...@doesrobbiedream/orchestrator-client@1.0.2) (2021-07-29)





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.1.1
* **@doesrobbiedream/orchestrator-commons:** upgraded to 1.0.2

## @doesrobbiedream/orchestrator-client [1.0.1](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/orchestrator-client@1.0.0...@doesrobbiedream/orchestrator-client@1.0.1) (2021-06-28)





### Dependencies

* **@doesrobbiedream/nest-core:** upgraded to 1.1.0
* **@doesrobbiedream/orchestrator-commons:** upgraded to 1.0.1

# @doesrobbiedream/orchestrator-client 1.0.0 (2021-06-17)


### Features

* **master:** automated library publishing ([b85d932](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b85d93299a57fad10f0de27c35b27e547f93c657))





### Dependencies

* **@doesrobbiedream/ts-utils:** upgraded to 1.0.0
* **@doesrobbiedream/nest-core:** upgraded to 1.0.0
* **@doesrobbiedream/orchestrator-commons:** upgraded to 1.0.0
