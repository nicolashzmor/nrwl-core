import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RefreshTokenSchema, UserSchema } from '../declarations/schemas'
import { PolymorphicUserProviders } from '../declarations/user.schema-providers'
import { UserRepository } from './user.repository'
import { LocalUserRepository } from './local-user.repository'
import { OAuthUserRepository } from './oauth-user.repository'
import { MONGO_CONNECTION_PROVIDER } from '../declarations/db_connection.declarations'
import { ConfigModule } from '@nestjs/config'
import { RefreshTokenRepository } from './refresh-token.repository'

@Module({
  imports: [
    ConfigModule,
    MongooseModule.forFeature(
      [
        { name: 'User', schema: UserSchema },
        { name: 'RefreshToken', schema: RefreshTokenSchema }
      ],
      'authentication-server'
    )
  ],
  providers: [
    MONGO_CONNECTION_PROVIDER,
    ...PolymorphicUserProviders,
    UserRepository,
    LocalUserRepository,
    OAuthUserRepository,
    RefreshTokenRepository
  ],
  exports: [
    ...PolymorphicUserProviders,
    UserRepository,
    LocalUserRepository,
    OAuthUserRepository,
    RefreshTokenRepository
  ]
})
export class RepositoriesModule {}
