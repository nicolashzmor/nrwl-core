import { DynamicModule, Module, Provider } from '@nestjs/common'
import { S3FileUploadService } from './s3-file-upload.service'
import { FileUploadModuleValues } from './file-upload-module.values'
import ModuleOptionsInterface = FileUploadModuleValues.ModuleOptionsInterface
import AsyncModuleOptionsInterface = FileUploadModuleValues.AsyncModuleOptionsInterface
import INSTANCE_OPTIONS = FileUploadModuleValues.INSTANCE_OPTIONS

@Module({})
export class FileUploadModule {
  public static register(options: ModuleOptionsInterface): DynamicModule {
    return {
      module: FileUploadModule,
      providers: [
        {
          provide: INSTANCE_OPTIONS,
          useValue: options
        },
        S3FileUploadService
      ],
      exports: [S3FileUploadService]
    }
  }

  public static registerAsync(options: AsyncModuleOptionsInterface): DynamicModule {
    return {
      global: true,
      module: FileUploadModule,
      imports: options.imports,
      providers: [this.registerAsyncOptions(options), S3FileUploadService],
      exports: [S3FileUploadService]
    }
  }

  private static registerAsyncOptions(options: AsyncModuleOptionsInterface): Provider {
    return options.useFactory
      ? {
          provide: INSTANCE_OPTIONS,
          useFactory: options.useFactory,
          inject: options.inject || []
        }
      : {
          provide: INSTANCE_OPTIONS,
          useValue: {},
          inject: options.inject || []
        }
  }
}
