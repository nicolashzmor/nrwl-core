import { Inject, Injectable } from '@nestjs/common'
import { AuthenticationServerApi } from '../declarations/authentication-server.api'
import { UserRepository } from '../repositories/user.repository'
import { LocalUserRepository } from '../repositories/local-user.repository'
import {
  Authentication,
  ExpiredCredentialsException,
  InvalidCredentialsException
} from '@doesrobbiedream/authentication-commons'
import { LocalUserDocument } from '../declarations/schemas'
import { compareSync } from 'bcrypt-nodejs'
import { sign } from 'jsonwebtoken'
import { ConfigService } from '@nestjs/config'
import { randHex } from '@doesrobbiedream/ts-utils'
import { RefreshTokenRepository } from '../repositories/refresh-token.repository'
import add from 'date-fns/add'
import GetCredentialDTO = AuthenticationServerApi.DTOs.GetCredentialDTO
import LocalUser = Authentication.LocalUser
import SignCredentialsDTO = AuthenticationServerApi.DTOs.SignCredentialsDTO
import RefreshTokenDTO = AuthenticationServerApi.DTOs.RefreshTokenDTO

@Injectable()
export class CredentialsManagerService {
  constructor(
    @Inject(ConfigService) protected config: ConfigService,
    @Inject(UserRepository) protected users: UserRepository,
    @Inject(LocalUserRepository) protected localUsers: LocalUserRepository,
    @Inject(RefreshTokenRepository) protected refreshTokens: RefreshTokenRepository
  ) {}

  async validateUserCredentials({ username, password }: GetCredentialDTO) {
    const user: LocalUser = await (this.localUsers.findByCondition({
      $or: [{ username: username }, { email: username }]
    }) as Promise<LocalUserDocument>).then((u) => u[0])

    if (!user || !compareSync(password, user.password)) {
      throw new InvalidCredentialsException()
    }

    return user
  }

  async signUserCredentials(user_data: SignCredentialsDTO) {
    const refresh_token = randHex(32)
    const jwt = sign({ ...user_data, refresh_token }, this.config.get<string>('JWT_SECRET'), {
      expiresIn: Number(this.config.get<string>('JWT_EXPIRES_IN')) || 300
    })
    await this.recordRefreshToken(user_data._id, jwt, refresh_token)
    return jwt
  }

  async refreshUserJWT({ _id, refresh_token, jwt }: RefreshTokenDTO) {
    const record = await this.refreshTokens
      .findByCondition({
        user: _id,
        refresh_token,
        jwt,
        expires: { $gt: new Date() },
        valid: true
      })
      .then((r) => r[0])

    if (!record) {
      throw new ExpiredCredentialsException()
    }

    await this.refreshTokens.update(record._id, { valid: false })
    return this.signUserCredentials({ _id })
  }

  protected async recordRefreshToken(_id: string, jwt: string, refresh_token: string) {
    const secondsInAWeek = 604800
    return await this.refreshTokens.create({
      user: _id,
      refresh_token,
      jwt,
      expires: add(new Date(), {
        seconds: Number(this.config.get<string>('REFRESH_TOKEN_EXPIRES_IN')) || secondsInAWeek
      })
    })
  }
}
