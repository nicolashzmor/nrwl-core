import { CallHandler, ExecutionContext, Injectable, NestInterceptor, Scope } from '@nestjs/common'
import { plainToClass } from 'class-transformer'
import { Observable, of } from 'rxjs'
import { switchMap } from 'rxjs/operators'

@Injectable({
  scope: Scope.TRANSIENT
})
export class ResponseTransformerInterceptor implements NestInterceptor {
  constructor(protected transformTo: new () => any, protected transformerOptions?: any) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      switchMap((response) =>
        of(
          plainToClass(this.transformTo, response, {
            excludeExtraneousValues: true, // BY DEFAULT, ALL EXTRANEOUS VALUES ARE IGNORED
            ...this.transformerOptions
          })
        )
      )
    )
  }
}
