import Color from 'color'

export namespace ColorUtils {
  export function contrast(color: string, contrastLight = 'white', contrastDark = 'black'): string {
    const [light, dark] = [contrastRatio(color, contrastLight), contrastRatio(color, contrastDark)]
    return light > dark ? contrastLight : contrastDark
  }

  export function contrastRatio(front: string, back: string): number {
    const backLuminance = luminance(back) + 0.05
    const frontLuminance = luminance(front) + 0.05
    return Math.max(backLuminance, frontLuminance) / Math.min(backLuminance, frontLuminance)
  }

  export function luminance(color: string): number {
    const c = Color(color)
    const channels = [c.red(), c.green(), c.blue()]
    const ratios = [0.2126, 0.7152, 0.0722]
    return channels
      .map((c) => {
        c = c / 255
        return c < 0.03928 ? c / 12.92 : Math.pow((c + 0.055) / 1.055, 2.4)
      })
      .reduce((a, c, i) => a + ratios[i] * c, 0)
  }

  export function gradient(color: string): { [key: number]: string } {
    const c = Color(color)
    const matrix = [
      50,
      ...Array(9)
        .fill(null)
        .map((x, i) => (i + 1) * 100)
    ]
    return matrix.reduce((gradient, darkness) => {
      return { ...gradient, ...{ [darkness]: c.lightness(darkness / 10).hex() } }
    }, {})
  }
}
