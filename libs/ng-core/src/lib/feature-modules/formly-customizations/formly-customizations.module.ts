import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MaterialPasswordTypeComponent } from './material-password-type/material-password-type.component'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { ReactiveFormsModule } from '@angular/forms'
import { FormlyModule } from '@ngx-formly/core'
import { MatIconModule } from '@angular/material/icon'
import { MatTooltipModule } from '@angular/material/tooltip'
import { LayoutModule } from '@angular/cdk/layout'

@NgModule({
  declarations: [MaterialPasswordTypeComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    LayoutModule,
    FormlyModule,
    MatIconModule,
    MatTooltipModule
  ],
  exports: [MaterialPasswordTypeComponent]
})
export class FormlyCustomizationsModule {}
