import { ValidationError } from '@nestjs/common'
import { Request } from 'express'
import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import { PolymorphicValidationPipeOptions } from './polymorphic-validation-pipe-options.interface'
import { QualifiedValidationWithExceptionPipe } from '../validation/qualified-validation-with-exception-pipe.service'
import { DTOValidationException } from '../exception-handling/exceptions/dto-validation.exception'

export class PolymorphicValidationPipe extends QualifiedValidationWithExceptionPipe {
  constructor(protected options?: PolymorphicValidationPipeOptions) {
    super(options)
  }

  async transform<T extends Request & { polymorphic_discriminator: string }>(
    request: T
  ): Promise<T> {
    if (
      !request.polymorphic_discriminator ||
      !this.options?.validationClasses[request.polymorphic_discriminator]
    ) {
      return request
    }
    const toValidateObject = plainToClass(
      this.options?.validationClasses[request.polymorphic_discriminator],
      request.body
    )
    const validationErrors: ValidationError[] = (await validate(
      toValidateObject
    )) as ValidationError[]
    if (validationErrors.length > 0) {
      throw new DTOValidationException(validationErrors)
    }
    return request
  }
}
