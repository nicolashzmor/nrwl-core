import { Pipe, PipeTransform } from '@angular/core'
import { interval, Observable } from 'rxjs'
import { map, startWith } from 'rxjs/operators'

@Pipe({ name: 'loadingDots' })
export class LoadingDotsPipe implements PipeTransform {
  transform(value: string, lapse = 500): Observable<string> {
    return interval(lapse).pipe(
      startWith(0),
      map((index) =>
        Array(index % 4)
          .fill('.')
          .join('')
      ),
      map((dots) => value + dots)
    )
  }
}
