#!/bin/bash
#
cd "$PWD" || exit

npm run affected:lint -- --base="${LAST_SUCCESSFUL_PIPELINE}" --head=HEAD
npm run affected:test -- --base="${LAST_SUCCESSFUL_PIPELINE}" --head=HEAD
