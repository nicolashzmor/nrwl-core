import { Pipe, PipeTransform } from '@angular/core'
import { DomSanitizer, SafeHtml } from '@angular/platform-browser'
import DOMPurify from 'dompurify'

@Pipe({
  name: 'safeHtml',
  pure: true
})
export class SafeHtmlPipe implements PipeTransform {
  constructor(protected sanitizer: DomSanitizer) {}

  public transform(value: string | Node): SafeHtml {
    const sanitizedContent = DOMPurify.sanitize(value)
    return this.sanitizer.bypassSecurityTrustHtml(sanitizedContent)
  }
}
