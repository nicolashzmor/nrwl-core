export interface MongoDocument {
  _id: any
  createdAt: Date
  updatedAt: Date
}
