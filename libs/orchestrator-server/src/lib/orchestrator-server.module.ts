import { DynamicModule, Module } from '@nestjs/common'
import { OrchestratorServerService } from './orchestrator-server.service'
import { OrchestratorServerController } from './orchestrator-server.controller'
import { MongooseModule } from '@nestjs/mongoose'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { OrchestratorServerConfigs } from './orchestrator-server.configs'
import { EXCEPTION_FILTER_PROVIDER } from '@doesrobbiedream/nest-core'
import { OrchestratorExceptions } from '@doesrobbiedream/orchestrator-commons'
import MONGO_ROOT_CONFIG = OrchestratorServerConfigs.MONGO_ROOT_CONFIG
import ModuleConfig = OrchestratorServerConfigs.ModuleConfig
import ORCHESTRATOR_SERVER_CONFIG = OrchestratorServerConfigs.ORCHESTRATOR_SERVER_CONFIG
import PROXY_PROVIDER = OrchestratorServerConfigs.PROXY_PROVIDER

@Module({
  imports: [
    MongooseModule.forRootAsync({
      connectionName: 'orchestrator-server',
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => MONGO_ROOT_CONFIG(config),
      inject: [ConfigService]
    })
  ],
  providers: [OrchestratorServerService],
  controllers: [OrchestratorServerController]
})
export class OrchestratorServerModule {
  public static register(config: ModuleConfig): DynamicModule {
    return {
      global: true,
      module: OrchestratorServerModule,
      imports: [
        ...(config.imports || []),
        MongooseModule.forRootAsync({
          connectionName: 'orchestrator-server',
          imports: [ConfigModule],
          useFactory: (config: ConfigService) => MONGO_ROOT_CONFIG(config),
          inject: [ConfigService]
        })
      ],
      providers: [
        {
          provide: ORCHESTRATOR_SERVER_CONFIG,
          useValue: config
        },
        { ...config.proxy_provider, provide: PROXY_PROVIDER },
        OrchestratorServerService,
        EXCEPTION_FILTER_PROVIDER(OrchestratorExceptions.dictionary)
      ]
    }
  }
}
