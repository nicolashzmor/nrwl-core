export namespace ExceptionHandlingResponses {
  export type ErrorData = Record<string, any>
  export interface CommonException extends ErrorData {
    message: string
  }

  export interface ValidationError {
    property: string
    value?: any
    children?: ValidationError[]
    constraints?: Record<string, any>
  }

  export interface ValidationErrorData extends CommonException {
    input: Record<string, any>
    errors: ValidationError[]
  }

  export interface ExceptionResponse {
    status: number
    exception: string
    timestamp: string
    request_path: string
    data: ErrorData
  }
}
