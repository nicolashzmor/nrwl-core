import { Injectable, ValidationError, ValidationPipe, ValidationPipeOptions } from '@nestjs/common'
import { DTOValidationException } from '../exception-handling/exceptions/dto-validation.exception'

@Injectable()
export class QualifiedValidationWithExceptionPipe extends ValidationPipe {
  constructor(options?: ValidationPipeOptions) {
    super({
      ...{
        exceptionFactory: (errors: ValidationError[]) => new DTOValidationException(errors),
        forbidUnknownValues: true,
        transform: true,
        whitelist: true,
        forbidNonWhitelisted: true
      },
      ...options
    })
  }
}
