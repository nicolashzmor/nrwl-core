export function getBaseClassFunction(obj: any): any {
  const proto = Object.getPrototypeOf(obj)
  return proto.constructor === Object ? obj.constructor : getBaseClassFunction(proto)
}
