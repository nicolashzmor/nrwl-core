export namespace ExceptionHandlingDictionaries {
  export const DefaultDictionary: ErrorMessageDictionary = {
    DEFAULT: {
      status: 500,
      exception: 'INTERNAL_SERVER_ERROR',
      message: 'Something went wrong. Please, try again later or contact support.'
    },
    NOT_IMPLEMENTED_METHOD: {
      status: 500,
      exception: 'NOT_IMPLEMENTED_METHOD',
      message:
        'This method is not yet implemented. Please contact support if you think this is an error.'
    },
    400: {
      status: 400,
      exception: 'BAD_REQUEST',
      message:
        'Your request cannot be properly handled. Please, try again later or contact support.'
    },
    401: {
      status: 401,
      exception: 'UNAUTHORIZED',
      message: 'You have not been authorized to perform this request.'
    },
    404: {
      status: 404,
      exception: 'NOT_FOUND',
      message: "We can't. find the resource you're requesting."
    },
    500: {
      status: 500,
      exception: 'INTERNAL_SERVER_ERROR',
      message: 'Something went wrong. Please, try again later or contact support.'
    },
    MONGO_11000: {
      status: 409,
      exception: 'DUPLICATED_KEY',
      message: "We've found a duplicated record. Review your request and try again."
    }
  }

  export type ErrorMessageDictionary = Record<string | number, ErrorMessageDeclaration>

  export interface ErrorMessageDeclaration {
    status: number
    exception: string
    message: string
  }
}
