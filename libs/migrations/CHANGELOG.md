# @doesrobbiedream/migrations 1.0.0 (2021-06-17)


### Features

* **master:** automated library publishing ([b85d932](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b85d93299a57fad10f0de27c35b27e547f93c657))





### Dependencies

* **@doesrobbiedream/console:** upgraded to 1.0.0
