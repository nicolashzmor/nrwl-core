import { ThemifyConfigurationInterface } from './themify-configuration.interface'
import { APP_INITIALIZER, ModuleWithProviders, NgModule } from '@angular/core'
import { ThemifyService } from './themify.service'
import { CONFIG_TOKEN } from './config.token'
import { ThemifyColorDirective } from './themify-color.directive'

@NgModule({
  providers: [],
  declarations: [ThemifyColorDirective],
  exports: [ThemifyColorDirective]
})
export class ThemifyModule {
  static forRoot(configuration: ThemifyConfigurationInterface): ModuleWithProviders<ThemifyModule> {
    return {
      ngModule: ThemifyModule,
      providers: [
        { provide: CONFIG_TOKEN, useValue: configuration },
        ThemifyService,
        {
          provide: APP_INITIALIZER,
          useFactory: (themify: ThemifyService) => () => themify.initializeColors(),
          deps: [ThemifyService],
          multi: true
        }
      ]
    }
  }
}
