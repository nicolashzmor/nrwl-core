import { MongoRepository } from '@doesrobbiedream/nest-core'
import { UserDocument } from '../declarations/schemas'
import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose'

export class UserRepository extends MongoRepository<UserDocument> {
  constructor(@InjectModel('User') entity: Model<UserDocument>) {
    super(entity)
  }
}
