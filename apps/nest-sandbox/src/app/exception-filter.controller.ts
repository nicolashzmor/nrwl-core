import { Controller, Get } from '@nestjs/common'
import { Exception, FromRPCException } from '@doesrobbiedream/nest-core'
import { of, throwError } from 'rxjs'
import { catchError, switchMap } from 'rxjs/operators'

@Controller('/exception-filter')
export class ExceptionFilterController {
  @Get('plain-exception')
  regularException() {
    throw Error()
  }

  @Get('provided-exception')
  providedException() {
    return of(false).pipe(
      switchMap(() =>
        throwError(
          new Exception({
            status: 409,
            exception: 'EXCEPTION',
            message: 'This is a RCP Exception'
          })
        )
      ),
      catchError((e) => throwError(new FromRPCException(e)))
    )
  }
}
