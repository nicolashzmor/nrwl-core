## @doesrobbiedream/nest-core [1.1.2](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/nest-core@1.1.1...@doesrobbiedream/nest-core@1.1.2) (2021-07-29)


### Bug Fixes

* **nest-core:** inverse default options on response-transformer.interceptor.ts ([ce1904b](https://gitlab.com/doesrobbiedream/nrwl-core/commit/ce1904b3bb45eaed9e6461b90b818079e52da5b0))

## @doesrobbiedream/nest-core [1.1.1](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/nest-core@1.1.0...@doesrobbiedream/nest-core@1.1.1) (2021-07-29)


### Bug Fixes

* **next-core:** inverse default options on response-transformer.interceptor.ts ([bf93d9a](https://gitlab.com/doesrobbiedream/nrwl-core/commit/bf93d9a47c6200b376ff2a6be75cf4d088f51a64))

# @doesrobbiedream/nest-core [1.1.0](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/nest-core@1.0.0...@doesrobbiedream/nest-core@1.1.0) (2021-06-28)


### Features

* **authentication-client,authentication-commons,authentication-server,nest-core:** m ([f6a6099](https://gitlab.com/doesrobbiedream/nrwl-core/commit/f6a609934086444d1d8de836ce6db01aa7215eac))

# @doesrobbiedream/nest-core 1.0.0 (2021-06-17)


### Features

* **master:** automated library publishing ([b85d932](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b85d93299a57fad10f0de27c35b27e547f93c657))





### Dependencies

* **@doesrobbiedream/ts-utils:** upgraded to 1.0.0
