import { ExceptionHandlingResponses } from '../responses'
import { ExceptionHandlingDictionaries } from '../dictionaries'
import ExceptionResponse = ExceptionHandlingResponses.ExceptionResponse
import DefaultDictionary = ExceptionHandlingDictionaries.DefaultDictionary
import ErrorMessageDeclaration = ExceptionHandlingDictionaries.ErrorMessageDeclaration

export class FromRPCException implements ExceptionResponse {
  status: number
  exception: string
  data: ExceptionHandlingResponses.ErrorData
  request_path: string
  timestamp: string

  constructor(error: Partial<ExceptionResponse>) {
    this.status = error.status || DefaultDictionary.DEFAULT.status
    this.exception = error.exception || DefaultDictionary.DEFAULT.exception
    this.data = { message: DefaultDictionary.DEFAULT.message, ...error.data }
    this.request_path = error.request_path || ''
    this.timestamp = error.timestamp || new Date().toISOString()
  }

  public overwrite(declaration?: ErrorMessageDeclaration) {
    if (declaration) {
      this.status = declaration.status
      this.exception = declaration.exception
      this.data = { ...this.data, message: declaration.message }
    }
  }
}
