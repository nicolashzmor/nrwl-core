import { ExceptionHandlingDictionaries } from '@doesrobbiedream/nest-core'
import ErrorMessageDictionary = ExceptionHandlingDictionaries.ErrorMessageDictionary

export const AuthenticationExceptionsDictionary: ErrorMessageDictionary = {
  USER_NOT_FOUND: {
    status: 404,
    exception: 'USER_NOT_FOUND',
    message: 'The provided user cannot be found. Please check your data and try again.'
  },
  DUPLICATED_KEY: {
    status: 409,
    exception: 'DUPLICATED_USER',
    message: 'This user is already registered. Please, try signing in or contact support.'
  },
  INVALID_CREDENTIALS: {
    status: 401,
    exception: 'INVALID_CREDENTIALS',
    message: "We couldn't match the provided credentials. Please, try again."
  },
  EXPIRED_CREDENTIALS: {
    status: 401,
    exception: 'EXPIRED_CREDENTIALS',
    message: 'Your credentials have expired. Please, sign in again.'
  }
}
