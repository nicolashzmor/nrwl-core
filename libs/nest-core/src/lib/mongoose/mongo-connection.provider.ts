import { Provider } from '@nestjs/common'
import * as mongoose from 'mongoose'
import { ConnectOptions } from 'mongoose'
import { defer } from 'rxjs'
import { handleRetry } from '@nestjs/mongoose/dist/common/mongoose.utils'

export const DATABASE_CONNECTION = 'DATABASE_CONNECTION'

export function MongoConnectionProvider(
  uri: string,
  overwriteToken?: string,
  connectionOptions?: ConnectOptions
): Provider {
  return {
    provide: overwriteToken || DATABASE_CONNECTION,
    useFactory: async (): Promise<any> =>
      await defer(async () => {
        const options = { useNewUrlParser: true, useUnifiedTopology: true, ...connectionOptions }
        return mongoose.createConnection(uri, options)
      })
        .pipe(handleRetry(5, 5000))
        .toPromise(),
    inject: []
  }
}
