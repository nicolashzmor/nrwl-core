import { BehaviorSubject, interval, merge, NEVER, Observable, Subject } from 'rxjs'
import { map, publish, startWith, switchMap, takeUntil, tap } from 'rxjs/operators'
import { TimerModels } from './timer.models'

export class Timer {
  public values$: BehaviorSubject<TimerModels.TimerValues> = new BehaviorSubject<TimerModels.TimerValues>(
    null as any
  )
  protected options: TimerModels.TimerOptions
  protected forceDestroy$ = new Subject()
  protected controlButton$: BehaviorSubject<{
    running: boolean
    startFrom: number
  }> = new BehaviorSubject({
    running: Boolean(false),
    startFrom: 0
  })
  protected source$: Observable<any> = this.controlButton$.pipe(
    switchMap(({ running, startFrom }) =>
      running
        ? interval(this.options.interval).pipe(
            startWith(0),
            map((emission) => emission + startFrom)
          )
        : NEVER
    ),
    takeUntil(merge(this.forceDestroy$, this.destroyer$))
  )
  public clock$: Observable<any> = this.source$.pipe(
    map((value) => ({
      emissions: value + 1,
      time: Number((value + 1) * (this.options.interval || 1))
    })),
    tap((values) => this.values$.next(values)),
    takeUntil(merge(this.forceDestroy$, this.destroyer$))
  )

  constructor(
    public id: string,
    protected destroyer$: Observable<any>,
    options?: TimerModels.TimerOptions
  ) {
    this.options = { ...TimerModels.TimerDefaults, ...options }
    publish()(this.clock$).connect()
  }

  public get values() {
    return this.values$.value
  }

  public init() {
    this.reset()
  }

  public reset() {
    this.controlButton$.next({ running: true, startFrom: 0 })
  }

  public stop() {
    this.controlButton$.next({ running: false, startFrom: 0 })
  }

  public start() {
    this.controlButton$.next({
      running: true,
      startFrom: this.values$.value.emissions
    })
  }

  public pause() {
    this.controlButton$.next({ running: false, startFrom: 0 })
  }

  public destroy() {
    this.forceDestroy$.next()
  }
}
