import { Test } from '@nestjs/testing'
import { LocalUserRepository } from '../repositories/local-user.repository'
import { UserRepository } from '../repositories/user.repository'
import { EntityManagerService } from './entity-manager.service'
import { Authentication, UserNotFoundException } from '@doesrobbiedream/authentication-commons'
import LocalUser = Authentication.LocalUser

describe('Entity Manager Service', () => {
  let service: EntityManagerService
  let localUsers: LocalUserRepository
  let users: UserRepository
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        {
          provide: LocalUserRepository,
          useValue: {
            create: jest.fn()
          }
        },
        {
          provide: UserRepository,
          useValue: {
            findByCondition: jest.fn(),
            findAndUpdate: jest.fn(),
            remove: jest.fn()
          }
        },
        EntityManagerService
      ]
    }).compile()
    service = module.get(EntityManagerService)
    localUsers = module.get(LocalUserRepository)
    users = module.get(UserRepository)
  })
  describe('User CRUD', () => {
    describe('getUser', () => {
      it('should get a user reference by _id, username or email', async () => {
        // Find By Username
        jest.spyOn(users, 'findByCondition').mockResolvedValue({ _id: 1 })
        const userByUsername = await service.getUser({ username: 'username' })
        expect(users.findByCondition).toHaveBeenCalledWith({
          $or: [{ _id: undefined }, { username: 'username' }, { email: undefined }]
        })
        expect(userByUsername).toBeDefined()
        // Find By ID
        const userByID = await service.getUser({ _id: '1234' })
        expect(users.findByCondition).toHaveBeenCalledWith({
          $or: [{ _id: '1234' }, { username: undefined }, { email: undefined }]
        })
        expect(userByID).toBeDefined()
        // Find By Email
        const userByEmail = await service.getUser({ email: 'some@email.com' })
        expect(users.findByCondition).toHaveBeenCalledWith({
          $or: [{ _id: undefined }, { username: undefined }, { email: 'some@email.com' }]
        })
        expect(userByEmail).toBeDefined()
      })
      it('should throw an error if not user is found', async () => {
        jest.spyOn(users, 'findByCondition').mockResolvedValue([])
        await service
          .getUser({ username: 'username' })
          .catch((error) => expect(error.constructor).toBe(UserNotFoundException))
      })
    })
    describe('listUser', () => {
      it('should be a defined method', () => {
        expect(service.listUsers).toBeDefined()
      })
    })
    describe('createLocalUser', () => {
      it('should complete the user automatic fields', async () => {
        jest.spyOn(users, 'findByCondition').mockResolvedValue([])
        jest.spyOn(localUsers, 'create').mockImplementation((data: LocalUser) => {
          expect(data.username).toBeDefined()
          expect(data.validated).toBeFalsy()
          expect(data.validation_token).toBeDefined()
          expect(data.validation_token_expiration_date.constructor).toEqual(Date)
          return new Promise((resolve) => resolve(data))
        })
        await service.createLocalUser({
          first_name: '',
          last_name: '',
          email: '',
          password: ''
        })
      })
    })
    describe('updateUser', () => {
      it('should throw error if user not found', async () => {
        jest.spyOn(users, 'findAndUpdate').mockResolvedValue(null)
        await service
          .updateUser({ _id: '1234', first_name: 'John' })
          .catch((e) => expect(e.constructor).toBe(UserNotFoundException))
      })
    })
    describe('removeUser', () => {
      it('should throw error if user not found', async () => {
        jest.spyOn(users, 'remove').mockResolvedValue(null)
        await service
          .removeUser('1234')
          .catch((e) => expect(e.constructor).toBe(UserNotFoundException))
      })
    })
    describe('validateUserEmail', () => {
      it('should be a defined method', () => {
        expect(service.validateUserEmail).toBeDefined()
      })
    })
    describe('createEmailValidationToken', () => {
      it('should be a defined method', () => {
        expect(service.createEmailValidationToken).toBeDefined()
      })
    })
    describe('createResetPasswordToken', () => {
      it('should be a defined method', () => {
        expect(service.createResetPasswordToken).toBeDefined()
      })
    })

    describe('resetPassword', () => {
      it('should be a defined method', () => {
        expect(service.resetPassword).toBeDefined()
      })
    })
  })
})
