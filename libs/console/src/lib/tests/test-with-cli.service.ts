import { Command, Console } from '../decorators'

@Console()
export class CliWithDecorator {
  @Command({
    command: 'command <arg1> <arg2>',
    alias: 'c',
    description: 'This is a test',
    options: [{ flags: '--optional <value>', required: true }]
  })
  testCommand(arg1: string, arg2: string, options: { optional: string }): void {
    console.log('CLI Test With Decorator', arg1, arg2, options)
  }
}
