import { RegisteredException } from '@doesrobbiedream/nest-core'

export class ExpiredCredentialsException extends RegisteredException {
  exception = 'EXPIRED_CREDENTIALS'

  constructor(public data?: Record<string, any>) {
    super()
  }
}
