import { Component, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core'
import { FileSystemFileEntry, NgxFileDropEntry } from 'ngx-file-drop'
import { from } from 'rxjs'
import { map, take } from 'rxjs/operators'
import { ImageFileParserService } from '../image-file-parser.service'

@Component({
  selector: 'drd-drop-files',
  templateUrl: './drop-files.component.html',
  styleUrls: ['./drop-files.component.scss']
})
export class DropFilesComponent {
  @Input() contentText = 'Drop files here'
  @Input() buttonText = 'or Browse Files'
  @HostBinding('class.overall-drop-zone')
  @Input()
  overallDropZone!: boolean

  @HostBinding('class.overall-drop-zone--show-on-file-over')
  @Input()
  showOnFileOver = true

  @HostBinding('class.overall-drop-zone--active')
  overallDropZoneActive!: boolean

  @Output() filesDropped: EventEmitter<
    { file: File; data: string | ArrayBuffer }[]
  > = new EventEmitter<{ file: File; data: string | ArrayBuffer }[]>()

  protected dragEventTarget!: any

  constructor(protected blobParser: ImageFileParserService) {}

  @HostListener('window:dragenter', ['$event'])
  onWindowDraggingOver(event: DragEvent) {
    const dt: any = event.dataTransfer
    this.dragEventTarget = event.target
    if (dt.items.length) {
      this.overallDropZoneActive = true
    }
  }

  @HostListener('window:dragleave', ['$event'])
  onWindowDraggingOut(event: Event) {
    if (event.target === this.dragEventTarget || event.target === document) {
      window.setTimeout(() => (this.overallDropZoneActive = false), 50)
    }
  }

  dropped(files: NgxFileDropEntry[]) {
    this.overallDropZoneActive = false
    from(this.recursiveListAllFileEntry(files))
      .pipe(take(1))
      .subscribe((results) => this.filesDropped.emit(results))
  }

  recursiveListAllFileEntry(
    drop: NgxFileDropEntry[]
  ): Promise<{ file: File; data: string | ArrayBuffer }[]> {
    const promises: Array<Promise<any>> = drop.reduce(
      (files: Array<Promise<any>>, d: NgxFileDropEntry) => {
        if (d.fileEntry.isDirectory) {
          throw Error(
            'Directory Uploads are not yet supported. Please, upload only a list of file.'
          )
        }
        const entry = d.fileEntry as FileSystemFileEntry
        return [
          ...files,
          new Promise((resolve) => {
            entry.file((f: File) =>
              this.blobParser
                .readImage(f)
                .pipe(
                  take(1),
                  map((parsed) => ({
                    file: f,
                    data: parsed
                  }))
                )
                .subscribe((data) => resolve(data))
            )
          })
        ]
      },
      []
    )
    return Promise.all(promises)
  }
}
