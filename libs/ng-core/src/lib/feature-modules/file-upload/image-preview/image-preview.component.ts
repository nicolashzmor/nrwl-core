import { ChangeDetectionStrategy, Component, Input } from '@angular/core'

@Component({
  selector: 'drd-image-preview',
  templateUrl: './image-preview.component.html',
  styleUrls: ['./image-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImagePreviewComponent {
  @Input() imageSrc!: string
  @Input() imageFile!: File
  @Input() restrictedSize = false
}
