import { createParamDecorator, ExecutionContext } from '@nestjs/common'

export const PolymorphicRequest = createParamDecorator(
  (param_key: string, ctx: ExecutionContext) => {
    const req = ctx.switchToHttp().getRequest()
    req.polymorphic_discriminator = req.params[param_key]
    return req
  }
)
