import { ConfigService } from '@nestjs/config'
import { Provider } from '@nestjs/common'
import { createConnection } from 'mongoose'

export const DB_CONNECTION = 'DB_CONNECTION'

export const MONGO_ROOT_CONFIG = (config: ConfigService) => ({
  uri: `${config.get<string>('AUTHENTICATION_DB_HOST') || 'mongodb://mongo:27017'}`,
  dbName: config.get<string>('AUTHENTICATION_DB_NAME') || 'authentication',
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false
})

export const MONGO_CONNECTION_PROVIDER: Provider = {
  provide: DB_CONNECTION,
  useFactory: (config: ConfigService) => {
    const { uri, dbName, ...rest } = MONGO_ROOT_CONFIG(config)
    return createConnection(`${uri}/${dbName}`, rest)
  },
  inject: [ConfigService]
}
