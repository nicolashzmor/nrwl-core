export interface SchemaInterface {
  library: string
  all: boolean
  exclude: string
}
