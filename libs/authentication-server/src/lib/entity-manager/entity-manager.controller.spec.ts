import { EntityManagerService } from './entity-manager.service'
import { EntityManagerController } from './entity-manager.controller'
import { Test } from '@nestjs/testing'
import {
  DTOValidationException,
  QualifiedValidationWithExceptionPipe
} from '@doesrobbiedream/nest-core'
import { ArgumentMetadata } from '@nestjs/common'
import { AuthenticationServerApi } from '../declarations/authentication-server.api'
import LocalUserCreateDTO = AuthenticationServerApi.DTOs.LocalUserCreateDTO
import GetUserDTO = AuthenticationServerApi.DTOs.GetUserDTO
import RemoveUserDTO = AuthenticationServerApi.DTOs.RemoveUserDTO
import UpdatableFields = AuthenticationServerApi.DTOs.UpdatableFields
import ResetPasswordDTO = AuthenticationServerApi.DTOs.ResetPasswordDTO
import NewResetPasswordTokenDTO = AuthenticationServerApi.DTOs.CreateResetPasswordTokenDTO
import ValidateUserDTO = AuthenticationServerApi.DTOs.ValidateUserDTO

describe('authentication.entity-manager', () => {
  let controller: EntityManagerController
  let service: EntityManagerService
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        {
          provide: EntityManagerService,
          useValue: {
            createLocalUser: jest.fn(),
            getUser: jest.fn(),
            listUsers: jest.fn(),
            removeUser: jest.fn(),
            updateUser: jest.fn(),
            resetPassword: jest.fn(),
            createResetPasswordToken: jest.fn(),
            validateUserEmail: jest.fn(),
            createEmailValidationToken: jest.fn()
          }
        }
      ],
      controllers: [EntityManagerController]
    }).compile()
    controller = module.get(EntityManagerController)
    service = module.get(EntityManagerService)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  describe('user', () => {
    describe(':create', () => {
      it('should call createLocalUser and handle returned object without intervention', async () => {
        jest
          .spyOn(service, 'createLocalUser')
          .mockResolvedValue(<any>{ data: 'UNTOUCHED_RESPONSE' })
        const response: any = await controller.createUser({} as any) // NOT INTERESTED IN DTO
        expect(service.createLocalUser).toHaveBeenCalled()
        expect(response.data).toEqual('UNTOUCHED_RESPONSE')
      })

      it('should validate LocalUserCreateDTO', async () => {
        const target = new QualifiedValidationWithExceptionPipe()
        const meta: ArgumentMetadata = {
          type: 'body',
          metatype: LocalUserCreateDTO,
          data: ''
        }
        await target
          .transform(<LocalUserCreateDTO>{}, meta)
          .catch((err: DTOValidationException) => {
            expect(err.constructor).toEqual(DTOValidationException)
            expect(err.data.errors).toHaveLength(4) // THE AMOUNT OF REQUIRED FIELDS
          })
      })
    })
    describe(':get', () => {
      it('should call getUser and handle returned object without intervention', async () => {
        jest.spyOn(service, 'getUser').mockResolvedValue(<any>{ data: 'UNTOUCHED_RESPONSE' })
        const response: any = await controller.getUser(<any>{})
        expect(service.getUser).toHaveBeenCalled()
        expect(response.data).toEqual('UNTOUCHED_RESPONSE')
      })
      it('should validate GetUserDTO', async () => {
        const target = new QualifiedValidationWithExceptionPipe()
        const meta: ArgumentMetadata = {
          type: 'body',
          metatype: GetUserDTO,
          data: ''
        }
        await target.transform(<GetUserDTO>{}, meta).catch((err: DTOValidationException) => {
          expect(err.constructor).toEqual(DTOValidationException)
          expect(err.data.errors).toHaveLength(1) // THE AMOUNT OF REQUIRED FIELDS
        })
      })
    })
    describe(':list', () => {
      it('should call listUsers and handle returned object without intervention', async () => {
        jest.spyOn(service, 'listUsers').mockResolvedValue(<any>{ data: 'UNTOUCHED_RESPONSE' })
        const response: any = await controller.getAllUsers({} as any) // NOT INTERESTED IN DTO
        expect(service.listUsers).toHaveBeenCalled()
        expect(response.data).toEqual('UNTOUCHED_RESPONSE')
      })
    })
    describe(':remove', () => {
      it('should call removeUser and handle returned object without intervention', async () => {
        jest.spyOn(service, 'removeUser').mockResolvedValue(<any>{ data: 'UNTOUCHED_RESPONSE' })
        const response: any = await controller.removeUser(<any>{})
        expect(service.removeUser).toHaveBeenCalled()
        expect(response.data).toEqual('UNTOUCHED_RESPONSE')
      })
      it('should validate RemoveUserDTO', async () => {
        const target = new QualifiedValidationWithExceptionPipe()
        const meta: ArgumentMetadata = {
          type: 'body',
          metatype: RemoveUserDTO,
          data: ''
        }
        await target.transform(<RemoveUserDTO>{}, meta).catch((err: DTOValidationException) => {
          expect(err.constructor).toEqual(DTOValidationException)
          expect(err.data.errors).toHaveLength(1) // THE AMOUNT OF REQUIRED FIELDS
        })
      })
    })
    describe(':update', () => {
      it('should call updateUser and handle returned object without intervention', async () => {
        jest.spyOn(service, 'updateUser').mockResolvedValue(<any>{ data: 'UNTOUCHED_RESPONSE' })
        const response: any = await controller.updateUser(<any>{})
        expect(service.updateUser).toHaveBeenCalled()
        expect(response.data).toEqual('UNTOUCHED_RESPONSE')
      })
      it('should validate UserCreateUpdatableFields', async () => {
        let capturedErrors = 0
        const target = new QualifiedValidationWithExceptionPipe()
        const meta: ArgumentMetadata = {
          type: 'body',
          metatype: UpdatableFields,
          data: ''
        }
        await target.transform(<UpdatableFields>{}, meta).catch((err: DTOValidationException) => {
          expect(err.constructor).toEqual(DTOValidationException)
          expect(err.data.errors).toHaveLength(1) // THE AMOUNT OF REQUIRED FIELDS
          capturedErrors++
        })
        await target
          .transform(
            <UpdatableFields>{
              _id: 'some_id',
              unknown: 12
            },
            meta
          )
          .catch((err: DTOValidationException) => {
            expect(err.constructor).toEqual(DTOValidationException)
            expect(err.data.errors).toHaveLength(1) // THE AMOUNT OF REQUIRED FIELDS
            capturedErrors++
          })
        expect(capturedErrors).toEqual(2)
      })
    })
  })
  describe('user.password', () => {
    describe(':reset', () => {
      it('should call resetPassword and handle returned object without intervention', async () => {
        jest.spyOn(service, 'resetPassword').mockResolvedValue(<any>{ data: 'UNTOUCHED_RESPONSE' })
        const response: any = await controller.resetPassword({} as any) // NOT INTERESTED IN DTO
        expect(service.resetPassword).toHaveBeenCalled()
        expect(response.data).toEqual('UNTOUCHED_RESPONSE')
      })

      it('should validate ResetPasswordDTO', async () => {
        const target = new QualifiedValidationWithExceptionPipe()
        const meta: ArgumentMetadata = {
          type: 'body',
          metatype: ResetPasswordDTO,
          data: ''
        }
        await target.transform(<ResetPasswordDTO>{}, meta).catch((err: DTOValidationException) => {
          expect(err.constructor).toEqual(DTOValidationException)
          expect(err.data.errors).toHaveLength(2) // THE AMOUNT OF REQUIRED FIELDS
        })
      })
    })
  })
  describe('user.reset-password-token', () => {
    describe(':create', () => {
      it('should call createResetPasswordToken and handle returned object without intervention', async () => {
        jest
          .spyOn(service, 'createResetPasswordToken')
          .mockResolvedValue(<any>{ data: 'UNTOUCHED_RESPONSE' })
        const response: any = await controller.newResetPasswordToken({} as any) // NOT INTERESTED IN DTO
        expect(service.createResetPasswordToken).toHaveBeenCalled()
        expect(response.data).toEqual('UNTOUCHED_RESPONSE')
      })

      it('should validate NewResetPasswordTokenDTO', async () => {
        const target = new QualifiedValidationWithExceptionPipe()
        const meta: ArgumentMetadata = {
          type: 'body',
          metatype: NewResetPasswordTokenDTO,
          data: ''
        }
        await target
          .transform(<NewResetPasswordTokenDTO>{}, meta)
          .catch((err: DTOValidationException) => {
            expect(err.constructor).toEqual(DTOValidationException)
            expect(err.data.errors).toHaveLength(1) // THE AMOUNT OF REQUIRED FIELDS
          })
      })
    })
  })
  describe('user.email-validation-token', () => {
    describe(':create', () => {
      it('should call createEmailValidationToken and handle returned object without intervention', async () => {
        jest
          .spyOn(service, 'createEmailValidationToken')
          .mockResolvedValue(<any>{ data: 'UNTOUCHED_RESPONSE' })
        const response: any = await controller.createUserEmailValidationToken({} as any) // NOT INTERESTED IN DTO
        expect(service.createEmailValidationToken).toHaveBeenCalled()
        expect(response.data).toEqual('UNTOUCHED_RESPONSE')
      })

      it('should validate NewResetPasswordTokenDTO', async () => {
        const target = new QualifiedValidationWithExceptionPipe()
        const meta: ArgumentMetadata = {
          type: 'body',
          metatype: NewResetPasswordTokenDTO,
          data: ''
        }
        await target
          .transform(<NewResetPasswordTokenDTO>{}, meta)
          .catch((err: DTOValidationException) => {
            expect(err.constructor).toEqual(DTOValidationException)
            expect(err.data.errors).toHaveLength(1) // THE AMOUNT OF REQUIRED FIELDS
          })
      })
    })
  })
  describe('user.email', () => {
    describe(':validate', () => {
      it('should call validateUserEmail and handle returned object without intervention', async () => {
        jest
          .spyOn(service, 'validateUserEmail')
          .mockResolvedValue(<any>{ data: 'UNTOUCHED_RESPONSE' })
        const response: any = await controller.validateUserEmail({} as any) // NOT INTERESTED IN DTO
        expect(service.validateUserEmail).toHaveBeenCalled()
        expect(response.data).toEqual('UNTOUCHED_RESPONSE')
      })

      it('should validate ValidateUserDTO', async () => {
        const target = new QualifiedValidationWithExceptionPipe()
        const meta: ArgumentMetadata = {
          type: 'body',
          metatype: ValidateUserDTO,
          data: ''
        }
        await target.transform(<ValidateUserDTO>{}, meta).catch((err: DTOValidationException) => {
          expect(err.constructor).toEqual(DTOValidationException)
          expect(err.data.errors).toHaveLength(1) // THE AMOUNT OF REQUIRED FIELDS
        })
      })
    })
  })
})
