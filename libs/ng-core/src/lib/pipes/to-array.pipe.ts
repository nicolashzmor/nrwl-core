import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'toArray'
})
export class ToArrayPipe implements PipeTransform {
  transform<T>(value: { [key: string]: T }): T[] {
    return value && Object.values(value)
  }
}
