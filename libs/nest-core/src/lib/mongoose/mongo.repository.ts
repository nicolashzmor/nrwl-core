import {
  EnforceDocument,
  FilterQuery,
  Model,
  QueryOptions,
  QueryWithHelpers,
  UpdateQuery,
  UpdateWithAggregationPipeline
} from 'mongoose'
import { MongooseDeclarations } from './declarations'
import { MongoException } from '../exception-handling/exceptions/mongo.exception'
import MongoRepositoryInterface = MongooseDeclarations.MongoRepositoryInterface

export abstract class MongoRepository<T> implements MongoRepositoryInterface<T> {
  constructor(protected entity: Model<T>) {}

  create(data: T): Promise<T> {
    return this.entity.create(data).catch((e) => {
      throw new MongoException(e)
    })
  }

  findAll(): QueryWithHelpers<Array<EnforceDocument<T, any>>, EnforceDocument<T, any>, any> {
    return this.entity.find().catch((e) => {
      throw new MongoException(e)
    })
  }

  findByCondition(
    filter: FilterQuery<T>,
    projection?: any | null,
    options?: QueryOptions | null,
    callback?: (err: any, docs: EnforceDocument<T, any>[]) => void
  ): QueryWithHelpers<Array<EnforceDocument<T, any>>, EnforceDocument<T, any>, any> {
    return this.entity.find(filter, projection, options, callback).catch((e) => {
      throw new MongoException(e)
    })
  }

  findOneById(
    _id: string
  ): QueryWithHelpers<EnforceDocument<T, any> | null, EnforceDocument<T, any>, any> {
    return this.entity.findById(_id).catch((e) => {
      throw new MongoException(e)
    })
  }

  remove(
    _id: any,
    options?: QueryOptions
  ): QueryWithHelpers<EnforceDocument<T, any> | null, EnforceDocument<T, any>, any> {
    return this.entity.findByIdAndDelete(_id, options).catch((e) => {
      throw new MongoException(e)
    })
  }

  update(
    id: string,
    query: UpdateWithAggregationPipeline | UpdateQuery<T>,
    options?: QueryOptions,
    callback?: (err: any, doc: EnforceDocument<T, any> | null, res: any) => void
  ): QueryWithHelpers<EnforceDocument<T, any> | null, EnforceDocument<T, any>, any> {
    return this.entity
      .findByIdAndUpdate(id, query, { new: true, ...options }, callback)
      .catch((e) => {
        throw new MongoException(e)
      })
  }

  public findAndUpdate(
    filter: FilterQuery<T>,
    update: UpdateWithAggregationPipeline | UpdateQuery<T>,
    options?: QueryOptions,
    callback?: (err: any, doc: EnforceDocument<T, any> | null, res: any) => void
  ) {
    return this.entity
      .findOneAndUpdate(filter, update, { new: true, ...options }, callback)
      .catch((e) => {
        throw new MongoException(e)
      })
  }

  aggregate<R = any>(aggregateConditions: any[]): Promise<Array<R>> {
    return this.entity.aggregate(aggregateConditions).catch((e) => {
      throw new MongoException(e)
    })
  }
}
