import { Injectable } from '@angular/core'
import { Observable, Subject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ImageFileParserService {
  readImage(file: File): Observable<string | ArrayBuffer> {
    const reader: FileReader = new FileReader()
    const parsed$: Subject<string | ArrayBuffer> = new Subject<string | ArrayBuffer>()
    reader.onload = (_event: ProgressEvent<FileReader>) => {
      parsed$.next(_event.target?.result as any)
    }
    reader.readAsDataURL(file)
    return parsed$.asObservable()
  }
}
