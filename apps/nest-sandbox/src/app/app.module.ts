import { MiddlewareConsumer, Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { ExceptionFilterController } from './exception-filter.controller'
import {
  CORS_CONFIG,
  EXCEPTION_FILTER_PROVIDER,
  FromRPCException,
  MessageBrokerModule,
  MessageBrokerProxyService,
  MicroserviceMessagingDeclarations,
  RequestLoggerMiddleware
} from '@doesrobbiedream/nest-core'
import {
  OrchestratorClientModule,
  OrchestratorClientService,
  PROXY_PROVIDER
} from '@doesrobbiedream/orchestrator-client'
import { OrchestratorController } from './orchestrator.controller'
import { AuthenticationClientModule } from '@doesrobbiedream/authentication-client'
import { Transport } from '@nestjs/microservices'
import { AuthenticationOrchestrations } from './orchestrations/authentication.orchestrations'
import PROXY_TYPES = MicroserviceMessagingDeclarations.PROXY_TYPES
import MESSAGE_BROKER_TOKEN = MicroserviceMessagingDeclarations.MESSAGE_BROKER_TOKEN

@Module({
  imports: [
    ConfigModule,
    MessageBrokerModule.forRootAsync({
      inject: [ConfigService],
      factory: (config: ConfigService) =>
        new MessageBrokerProxyService({
          transport: Transport.REDIS,
          options: { url: config.get<string>('REDIS_URL') }
        }),
      imports: [ConfigModule]
    }),
    OrchestratorClientModule.register({
      options: {
        service: {
          exception: FromRPCException
        }
      },
      proxy_provider: {
        provide: PROXY_PROVIDER,
        useExisting: MESSAGE_BROKER_TOKEN
      }
    }),
    AuthenticationClientModule.forRoot({
      brokers: {
        [PROXY_TYPES.MESSAGE_BROKER]: {
          provide: 'MESSAGE_BROKER',
          useExisting: MESSAGE_BROKER_TOKEN
        },
        [PROXY_TYPES.ORCHESTRATOR]: {
          provide: 'ORCHESTRATOR',
          useExisting: OrchestratorClientService
        }
      },
      orchestrations: AuthenticationOrchestrations
    })
  ],
  controllers: [AppController, ExceptionFilterController, OrchestratorController],
  providers: [
    {
      provide: CORS_CONFIG,
      useValue: []
    },
    AppService,
    EXCEPTION_FILTER_PROVIDER()
  ]
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(RequestLoggerMiddleware).forRoutes('*')
  }
}
