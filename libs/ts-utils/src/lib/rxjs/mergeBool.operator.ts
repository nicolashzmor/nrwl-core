import { ObservableInput, ObservedValueOf, OperatorFunction } from 'rxjs'
import { mergeMap } from 'rxjs/operators'

export function mergeMapBool<T, O extends ObservableInput<boolean>>(
  project: O,
  concurrent?: number
): OperatorFunction<T, ObservedValueOf<O>> {
  return mergeMap((previous) => previous && project, concurrent)
}
