#!/bin/bash

if [[ "$CI_COMMIT_DESCRIPTION" =~ (.|^)+\[ci:apps:deploy_all\](.|$)+ ]]; then
  nx run-many --target=build --projects="$(nx affected:libs --all --plain --with-deps | sed -e 's/\s\+/,/g')"
else
  # TODO: REMOVE THE BUILD ALL ONCE FOUND AN EVIDENCE THAT SEMANTIC-RELEASE WILL WORK AS INTENDED
  nx run-many --target=build --projects="$(nx affected:libs --all --plain --with-deps | sed -e 's/\s\+/,/g')"
fi
