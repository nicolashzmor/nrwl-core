import { ISendMailOptions, MailerOptions } from '@nestjs-modules/mailer'
import { MailerAsyncOptions } from '@nestjs-modules/mailer/dist/interfaces/mailer-async-options.interface'
import { FactoryProvider } from '@nestjs/common'

export namespace NotificationServerDeclarations {
  export interface NotificationServerConfig {
    mailer_config: MailerOptions | MailerAsyncOptions
    transformers?: NotificationContextTransformers
  }

  export interface NotificationServerConfigAsync {
    mailer_config: MailerOptions | MailerAsyncOptions
    imports?: any[]
    transformersFactory?: FactoryProvider<NotificationContextTransformers>
  }

  export const NOTIFICATION_TRANSFORMER_TOKEN = 'NOTIFICATION_TRANSFORMER_TOKEN'
  export type NotificationContextTransformers = Record<
    string,
    (context: Record<string, any>) => ISendMailOptions
  >

  export interface EmailNotificationInput {
    transformer: string
    data: Record<string, any>
  }
}
