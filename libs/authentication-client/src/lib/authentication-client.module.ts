import { DynamicModule, Module } from '@nestjs/common'
import { OauthController } from './oauth.controller'
import { AuthenticationController } from './authentication.controller'
import { EXCEPTION_FILTER_PROVIDER } from '@doesrobbiedream/nest-core'
import { AuthenticationExceptionsDictionary } from '@doesrobbiedream/authentication-commons'
import { AuthenticationClientConfig } from './authentication-client-module.config'
import { AuthenticationClientProcedures } from './authentication-client.procedures'
import { ConfigModule } from '@nestjs/config'
import { LocalStrategy } from './strategies/local.strategy'
import { JwtStrategy } from './strategies/jwt.strategy'
import { RefreshTokenStrategy } from './strategies/refresh-token.strategy'
import AuthenticationClientModuleConfig = AuthenticationClientConfig.AuthenticationClientModuleConfig
import PROCEDURES_TOKEN = AuthenticationClientProcedures.PROCEDURES_TOKEN
import DEFAULT_PROCEDURES = AuthenticationClientProcedures.DEFAULT_PROCEDURES
import AUTHENTICATION_CLIENT_CONFIG = AuthenticationClientConfig.AUTHENTICATION_CLIENT_CONFIG
import BROKERS = AuthenticationClientConfig.BROKERS

@Module({
  controllers: [AuthenticationController, OauthController],
  providers: [EXCEPTION_FILTER_PROVIDER(AuthenticationExceptionsDictionary)]
})
export class AuthenticationClientModule {
  static forRoot(config: AuthenticationClientModuleConfig): DynamicModule {
    if (Object.keys(config.brokers || {}).length === 0) {
      throw Error('Authentication Client requires at least one message broker to work.')
    }
    const brokersProviders = Object.keys(config.brokers).map((k) => ({
      ...config.brokers[k],
      provide: k
    }))
    return {
      global: true,
      module: AuthenticationClientModule,
      imports: [ConfigModule],
      providers: [
        {
          provide: AUTHENTICATION_CLIENT_CONFIG,
          useValue: config
        },
        {
          provide: PROCEDURES_TOKEN,
          useValue: { ...DEFAULT_PROCEDURES, ...config.orchestrations }
        },
        ...brokersProviders,
        {
          provide: BROKERS,
          useFactory: (...brokers) => {
            return Object.keys(config.brokers).reduce(
              (br, k, i) => ({ ...br, [k]: brokers[i] }),
              {}
            )
          },
          inject: Object.keys(config.brokers)
        },
        LocalStrategy,
        // GoogleStrategy,
        JwtStrategy,
        RefreshTokenStrategy,
        EXCEPTION_FILTER_PROVIDER()
      ]
    }
  }
}
