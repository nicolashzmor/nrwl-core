import { OrchestratorDefinitions } from '@doesrobbiedream/orchestrator-commons'
import { MicroserviceMessagingDeclarations } from '@doesrobbiedream/nest-core'
import { AuthenticationClientProcedures } from '@doesrobbiedream/authentication-client'
import AUTHENTICATION_CLIENT_PROCEDURES = AuthenticationClientProcedures.AUTHENTICATION_CLIENT_PROCEDURES
import CollectedData = OrchestratorDefinitions.CollectedData
import Orchestration = MicroserviceMessagingDeclarations.Orchestration

export const AuthenticationOrchestrations: {
  [key in AUTHENTICATION_CLIENT_PROCEDURES]?: Orchestration
} = {
  [AUTHENTICATION_CLIENT_PROCEDURES.SIGN_UP]: new Orchestration('procedure')
    .setId('procedures/authentication/sign-up')
    .addStep({
      pattern: 'authentication.entity-manager.user:create',
      input: (collected: CollectedData) => collected.input
    })
    .addStep({
      pattern: 'notifications.email:send',
      input: (collected) => ({
        transformer: 'welcome-email',
        data: collected['authentication.entity-manager.user:create']
      })
    })
    .setResponse((collected) => {
      return collected['authentication.entity-manager.user:create'].user
    })
}
