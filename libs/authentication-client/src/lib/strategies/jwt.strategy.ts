import { Inject, Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'

import { AuthenticationClientConfig } from '../authentication-client-module.config'
import { AuthenticationClientProcedures } from '../authentication-client.procedures'
import { MicroserviceMessagingDeclarations } from '@doesrobbiedream/nest-core'
import { Authentication } from '@doesrobbiedream/authentication-commons'
import PROCEDURES_TOKEN = AuthenticationClientProcedures.PROCEDURES_TOKEN
import AUTHENTICATION_CLIENT_PROCEDURES = AuthenticationClientProcedures.AUTHENTICATION_CLIENT_PROCEDURES
import AUTHENTICATION_CLIENT_CONFIG = AuthenticationClientConfig.AUTHENTICATION_CLIENT_CONFIG
import AuthenticationClientModuleConfig = AuthenticationClientConfig.AuthenticationClientModuleConfig
import submitToHandler = MicroserviceMessagingDeclarations.submitToHandler
import Signature = Authentication.Signature

import BROKERS = AuthenticationClientConfig.BROKERS
import BrokersList = MicroserviceMessagingDeclarations.BrokersList

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    protected env: ConfigService,
    @Inject(PROCEDURES_TOKEN)
    protected procedures: Record<AUTHENTICATION_CLIENT_PROCEDURES, any>,
    @Inject(BROKERS)
    protected brokers: BrokersList,
    @Inject(AUTHENTICATION_CLIENT_CONFIG)
    protected config: AuthenticationClientModuleConfig
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: env.get('JWT_SECRET')
    })
  }

  async validate(payload: Signature) {
    return await submitToHandler(this.brokers, this.procedures.FIND_USER, {
      _id: payload._id
    }).toPromise()
  }
}
