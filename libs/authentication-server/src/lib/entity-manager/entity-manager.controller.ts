import { Body, Controller, Inject, UseInterceptors, UsePipes } from '@nestjs/common'
import { EntityManagerService } from './entity-manager.service'
import { MessagePattern } from '@nestjs/microservices'
import {
  QualifiedValidationWithExceptionPipe,
  ResponseTransformerInterceptor
} from '@doesrobbiedream/nest-core'

import { AuthenticationServerApi } from '../declarations/authentication-server.api'
import { FilterQuery } from 'mongoose'
import { UserDocument } from '../declarations/schemas'
import LocalUserCreateDTO = AuthenticationServerApi.DTOs.LocalUserCreateDTO
import UserCreateUpdatableFields = AuthenticationServerApi.DTOs.UpdatableFields
import NewResetPasswordTokenDTO = AuthenticationServerApi.DTOs.CreateResetPasswordTokenDTO
import ResetPasswordDTO = AuthenticationServerApi.DTOs.ResetPasswordDTO
import GetUserDTO = AuthenticationServerApi.DTOs.GetUserDTO
import RemoveUserDTO = AuthenticationServerApi.DTOs.RemoveUserDTO
import ValidateUserDTO = AuthenticationServerApi.DTOs.ValidateUserDTO
import UserResponse = AuthenticationServerApi.Responses.UserResponse
import NewPasswordResetToken = AuthenticationServerApi.Responses.NewPasswordResetToken
import CreateUserResponse = AuthenticationServerApi.Responses.CreateUserResponse
import CreateEmailValidationTokenDTO = AuthenticationServerApi.DTOs.CreateEmailValidationTokenDTO
import NewEmailValidationToken = AuthenticationServerApi.Responses.NewEmailValidationToken

@Controller()
export class EntityManagerController {
  constructor(@Inject(EntityManagerService) protected service: EntityManagerService) {}

  @MessagePattern('authentication.entity-manager.user:get')
  @UsePipes(QualifiedValidationWithExceptionPipe)
  @UseInterceptors(new ResponseTransformerInterceptor(UserResponse))
  getUser(@Body() query: GetUserDTO) {
    return this.service.getUser(query)
  }

  @MessagePattern('authentication.entity-manager.user:list')
  @UseInterceptors(new ResponseTransformerInterceptor(UserResponse))
  getAllUsers(@Body() query: FilterQuery<UserDocument>) {
    return this.service.listUsers(query)
  }

  @MessagePattern('authentication.entity-manager.user:create')
  @UseInterceptors(new ResponseTransformerInterceptor(CreateUserResponse))
  @UsePipes(QualifiedValidationWithExceptionPipe)
  createUser(@Body() user: LocalUserCreateDTO) {
    return this.service.createLocalUser(user)
  }

  @MessagePattern('authentication.entity-manager.user:update')
  @UsePipes(QualifiedValidationWithExceptionPipe)
  @UseInterceptors(new ResponseTransformerInterceptor(UserResponse))
  updateUser(@Body() user: UserCreateUpdatableFields) {
    return this.service.updateUser(user)
  }

  @MessagePattern('authentication.entity-manager.user:remove')
  @UsePipes(QualifiedValidationWithExceptionPipe)
  @UseInterceptors(new ResponseTransformerInterceptor(UserResponse))
  removeUser(@Body() { _id }: RemoveUserDTO) {
    return this.service.removeUser(_id)
  }

  @MessagePattern('authentication.entity-manager.user.password:reset')
  @UsePipes(QualifiedValidationWithExceptionPipe)
  @UseInterceptors(new ResponseTransformerInterceptor(UserResponse))
  resetPassword(@Body() { token, password }: ResetPasswordDTO) {
    return this.service.resetPassword(token, password)
  }

  @MessagePattern('authentication.entity-manager.user.reset-password-token:create')
  @UsePipes(QualifiedValidationWithExceptionPipe)
  @UseInterceptors(new ResponseTransformerInterceptor(NewPasswordResetToken))
  newResetPasswordToken(@Body() data: NewResetPasswordTokenDTO) {
    return this.service.createResetPasswordToken(data.email)
  }

  @MessagePattern('authentication.entity-manager.user.email-validation-token:create')
  @UsePipes(QualifiedValidationWithExceptionPipe)
  @UseInterceptors(new ResponseTransformerInterceptor(NewEmailValidationToken))
  createUserEmailValidationToken(@Body() { email }: CreateEmailValidationTokenDTO) {
    return this.service.createEmailValidationToken(email)
  }

  @MessagePattern('authentication.entity-manager.user.email:validate')
  @UsePipes(QualifiedValidationWithExceptionPipe)
  @UseInterceptors(new ResponseTransformerInterceptor(UserResponse))
  validateUserEmail(@Body() { token }: ValidateUserDTO) {
    return this.service.validateUserEmail(token)
  }
}
