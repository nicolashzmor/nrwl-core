import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core'
import { FieldType } from '@ngx-formly/material'
import { FormControl } from '@angular/forms'
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout'
import { Observable, Subscription } from 'rxjs'
import { map, tap } from 'rxjs/operators'
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay'
import { FormlyTemplateOptions } from '@ngx-formly/core'
import { ComponentPortal, ComponentType, TemplatePortal } from '@angular/cdk/portal'

export type PasswordTypeTemplateOptions = FormlyTemplateOptions & {
  passwordHintDialogComponent?: ComponentType<any>
  passwordHintDialogTemplate?: TemplateRef<any>
}

@Component({
  selector: 'drd-material-password-type-component',
  templateUrl: './material-password-type.component.html',
  styleUrls: ['./material-password-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MaterialPasswordTypeComponent extends FieldType implements OnDestroy {
  formControl!: FormControl
  to!: PasswordTypeTemplateOptions

  @ViewChild('passwordField', { static: false }) passwordField!: ElementRef
  public toggled = false
  public openedDialog = false
  public isMobile$: Observable<boolean>
  public overlayRef!: OverlayRef
  protected overlayConfiguration: Subscription

  constructor(
    protected overlay: Overlay,
    protected breakpoints: BreakpointObserver,
    protected $el: ElementRef,
    protected viewRef: ViewContainerRef,
    protected cdr: ChangeDetectorRef
  ) {
    super()
    this.isMobile$ = this.breakpoints
      .observe([Breakpoints.Handset])
      .pipe(map(({ matches }: any) => matches))
    this.overlayConfiguration = this.isMobile$
      .pipe(
        map((matches) =>
          matches
            ? new OverlayConfig({
                positionStrategy: this.overlay.position().global().centerHorizontally().top(),
                hasBackdrop: false
              })
            : new OverlayConfig({
                positionStrategy: this.overlay
                  .position()
                  .flexibleConnectedTo(this.$el.nativeElement)
                  .withPositions([
                    {
                      originX: 'end',
                      originY: 'center',
                      overlayX: 'start',
                      overlayY: 'center',
                      offsetX: 16
                    }
                  ]),
                hasBackdrop: false
              })
        ),
        tap((config: OverlayConfig) => (this.overlayRef = this.overlay.create(config)))
      )
      .subscribe()
  }

  get hasHintDialog() {
    return this.to.passwordHintDialogComponent || this.to.passwordHintDialogTemplate
  }

  ngOnDestroy() {
    super.ngOnDestroy()
    this.overlayConfiguration.unsubscribe()
    if (this.overlayRef.hasAttached()) {
      this.overlayRef.detach()
    }
  }

  public togglePasswordVisibility() {
    this.toggled = !this.toggled
    this.passwordField.nativeElement.type =
      this.passwordField.nativeElement.type === 'password' ? 'text' : 'password'
  }

  public togglePasswordHint() {
    const portal = this.portalConstructor(this.to)
    if (portal && !this.overlayRef.hasAttached()) {
      this.overlayRef.attach(portal)
      this.openedDialog = true
      this.cdr.markForCheck()
    } else {
      this.overlayRef.detach()
      this.openedDialog = false
      this.cdr.markForCheck()
    }
  }

  public hidePasswordHint() {
    if (this.overlayRef.hasAttached()) {
      setTimeout(() => {
        if (this.overlayRef.hasAttached()) {
          this.overlayRef.detach()
          this.openedDialog = false
          this.cdr.markForCheck()
        }
      }, 0)
    }
  }

  protected portalConstructor(to: PasswordTypeTemplateOptions) {
    if (to.passwordHintDialogComponent) {
      return new ComponentPortal(to.passwordHintDialogComponent)
    }
    if (to.passwordHintDialogTemplate) {
      return new TemplatePortal(to.passwordHintDialogTemplate, this.viewRef)
    }
    return null
  }
}
