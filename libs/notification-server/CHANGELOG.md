## @doesrobbiedream/notification-server [1.0.1](https://gitlab.com/doesrobbiedream/nrwl-core/compare/@doesrobbiedream/notification-server@1.0.0...@doesrobbiedream/notification-server@1.0.1) (2021-06-24)


### Bug Fixes

* **notification-server:** add try/catch statement to sendmail process ([d0becb0](https://gitlab.com/doesrobbiedream/nrwl-core/commit/d0becb0167fa1de31d68a928bd04fd1566a58d87))

# @doesrobbiedream/notification-server 1.0.0 (2021-06-17)


### Features

* **master:** automated library publishing ([b85d932](https://gitlab.com/doesrobbiedream/nrwl-core/commit/b85d93299a57fad10f0de27c35b27e547f93c657))





### Dependencies

* **@doesrobbiedream/ts-utils:** upgraded to 1.0.0
