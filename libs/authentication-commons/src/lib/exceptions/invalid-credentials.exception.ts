import { RegisteredException } from '@doesrobbiedream/nest-core'

export class InvalidCredentialsException extends RegisteredException {
  public exception = 'INVALID_CREDENTIALS'

  constructor(public data?: Record<string, any>) {
    super()
  }
}
