FROM docker:19.03.12
RUN apk update && apk upgrade && apk add curl bash docker-compose git yarn && yarn global add @nrwl/cli
SHELL ["/bin/bash", "-c"]
