import { Provider } from '@nestjs/common'
import { AuthenticationServer } from './authentication-server.declarations'
import { Connection } from 'mongoose'
import { LocalUserSchema, OAuthUserSchema, UserSchema } from './schemas'
import { DB_CONNECTION } from './db_connection.declarations'
import USER_TYPES = AuthenticationServer.USER_TYPES

export const PolymorphicUserProvidersFactoryDictionary: { [key in USER_TYPES]: Provider } = {
  [USER_TYPES.LOCAL]: {
    provide: USER_TYPES.LOCAL,
    useFactory: (connection: Connection) =>
      connection.model('User', UserSchema).discriminator(USER_TYPES.LOCAL, LocalUserSchema),
    inject: [DB_CONNECTION]
  },
  [USER_TYPES.OAUTH]: {
    provide: USER_TYPES.OAUTH,
    useFactory: (connection: Connection) =>
      connection.model('User', UserSchema).discriminator(USER_TYPES.OAUTH, OAuthUserSchema),
    inject: [DB_CONNECTION]
  }
}
export const PolymorphicUserProviders = Object.values(PolymorphicUserProvidersFactoryDictionary)
