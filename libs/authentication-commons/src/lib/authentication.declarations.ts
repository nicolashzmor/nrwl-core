export namespace Authentication {
  export enum OAUTH_PROVIDER {
    GOOGLE = 'google',
    FACEBOOK = 'facebook'
  }

  export interface User {
    first_name: string
    last_name: string
    email: string
    username: string
    validated: boolean
  }

  export interface LocalUser extends User {
    password: string
    password_reset_token?: string
    password_reset_token_expiration?: Date
    validation_token?: string
    validation_token_expiration_date: Date
  }

  export interface OAuthUser extends User {
    providers: OAuthProvider[]
  }

  export interface OAuthProvider {
    uuid: string
    accessToken: string
    provider: OAUTH_PROVIDER
    profile_data: Record<string, any>
  }

  export interface Signature {
    _id: string
    refresh_token: string
  }

  export interface Credentials {
    jwt: string
  }

  export interface RefreshToken {
    user: string
    refresh_token: string
    jwt: string
    expires: Date
    valid?: boolean
  }
}
