import { MicroserviceMessagingDeclarations } from '@doesrobbiedream/nest-core'

export namespace AuthenticationClientProcedures {
  import Orchestration = MicroserviceMessagingDeclarations.Orchestration
  import Message = MicroserviceMessagingDeclarations.Message
  export const PROCEDURES_TOKEN = 'AUTHENTICATION_CLIENT_PROCEDURES'

  export enum AUTHENTICATION_CLIENT_PROCEDURES {
    SIGN_UP = 'SIGN_UP',
    SIGN_IN = 'SIGN_IN',
    REFRESH_TOKEN = 'REFRESH_TOKEN',
    VALIDATE_EMAIL = 'VALIDATE_EMAIL',
    RESEND_EMAIL_VERIFICATION = 'RESEND_EMAIL_VERIFICATION',
    SET_NEW_PASSWORD = 'SET_NEW_PASSWORD',
    REQUEST_PASSWORD_RESET = 'REQUEST_PASSWORD_RESET',
    LIST_USERS = 'LIST_USERS',
    FIND_USER = 'FIND_USER'
  }

  export type MessageOrErrorThrow = MicroserviceMessagingDeclarations.Message | any
  export const DEFAULT_PROCEDURES: Record<AUTHENTICATION_CLIENT_PROCEDURES, MessageOrErrorThrow> = {
    FIND_USER: new Message('send', 'authentication.entity-manager.user:get'),
    LIST_USERS: new Message('send', 'authentication.entity-manager.user:list'),
    REFRESH_TOKEN: new Orchestration('procedure')
      .setId('procedures/authentication/refresh-token')
      .addStep({
        pattern: 'authentication.credentials-manager.credentials:refresh',
        input: ({ input }) => input
      })
      .setResponse(
        (collected) => collected['authentication.credentials-manager.credentials:refresh']
      ),
    REQUEST_PASSWORD_RESET: new Message(
      'send',
      'authentication.entity-manager.user.reset-password-token:create'
    ),
    RESEND_EMAIL_VERIFICATION: new Message(
      'send',
      'authentication.entity-manager.user.email-validation-token:create'
    ),
    SET_NEW_PASSWORD: new Message('send', 'authentication.entity-manager.user.password:reset'),
    SIGN_IN: new Orchestration('procedure')
      .setId('procedures/authentication/sign-in')
      .addStep({
        pattern: 'authentication.credentials-manager.credentials:get',
        input: ({ input }) => ({
          username: input.username || input.email,
          password: input.password
        })
      })
      .addStep({
        pattern: 'authentication.credentials-manager.credentials:sign',
        input: (collected) => {
          const user = collected['authentication.credentials-manager.credentials:get']
          return { _id: user._id }
        }
      })
      .setResponse((collected) => collected['authentication.credentials-manager.credentials:sign']),
    SIGN_UP: new Message('send', 'authentication.entity-manager.user:create'),
    VALIDATE_EMAIL: new Message('send', 'authentication.entity-manager.user.email:validate')
  }
}
