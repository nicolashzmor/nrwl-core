import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DeepEntityFactoryService } from './deep-entity-factory.service'

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [DeepEntityFactoryService]
})
export class DeepEntityFactoryModule {}
