import { Controller, Get, Request, UseGuards } from '@nestjs/common'
import { JwtAuthGuard } from '@doesrobbiedream/authentication-client'

@Controller()
export class AppController {
  @UseGuards(JwtAuthGuard)
  @Get('test-jwt')
  testJwt(@Request() { user }: any) {
    console.log(user)
  }
}
