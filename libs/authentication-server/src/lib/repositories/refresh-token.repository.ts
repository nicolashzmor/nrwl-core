import { MongoRepository } from '@doesrobbiedream/nest-core'
import { Authentication } from '@doesrobbiedream/authentication-commons'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import RefreshToken = Authentication.RefreshToken

export class RefreshTokenRepository extends MongoRepository<RefreshToken> {
  constructor(@InjectModel('RefreshToken') entity: Model<RefreshToken>) {
    super(entity)
  }
}
