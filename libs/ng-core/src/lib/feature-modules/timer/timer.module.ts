import { ModuleWithProviders, NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { TimerService } from './timer.service'

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: []
})
export class TimerModule {
  public static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: TimerModule,
      providers: [TimerService]
    }
  }
}
