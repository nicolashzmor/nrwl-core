import { ValidationPipeOptions } from '@nestjs/common'

export interface PolymorphicValidationPipeOptions extends ValidationPipeOptions {
  validationClasses: { [key: string]: new () => any }
}
