import { MicroserviceMessagingDeclarations } from '@doesrobbiedream/nest-core'
import { AuthenticationClientProcedures } from './authentication-client.procedures'
import {
  ClassProvider,
  ExistingProvider,
  FactoryProvider,
  ValueProvider
} from '@nestjs/common/interfaces/modules/provider.interface'

export namespace AuthenticationClientConfig {
  import AUTHENTICATION_CLIENT_PROCEDURES = AuthenticationClientProcedures.AUTHENTICATION_CLIENT_PROCEDURES
  import BrokersListProviders = MicroserviceMessagingDeclarations.BrokersListProviders
  import Message = MicroserviceMessagingDeclarations.Message
  import Orchestration = MicroserviceMessagingDeclarations.Orchestration

  export const MESSAGE_BROKER_PROVIDER = 'MESSAGE_BROKER_PROVIDER'
  export const AUTHENTICATION_CLIENT_CONFIG = 'AUTHENTICATION_CLIENT_CONFIG'
  export const BROKERS = 'BROKERS'

  export type Provider<T> =
    | ClassProvider<T>
    | ValueProvider<T>
    | FactoryProvider<T>
    | ExistingProvider<T>

  export interface AuthenticationClientModuleConfig {
    brokers: BrokersListProviders
    orchestrations: {
      [key in AUTHENTICATION_CLIENT_PROCEDURES]?: Message | Orchestration
    }
  }
}
