import { BehaviorSubject, combineLatest, merge, Observable, Subject } from 'rxjs'
import { Timer } from './timer'
import { v4 as uuidv4 } from 'uuid'
import _filter from 'lodash/filter'
import _forEach from 'lodash/forEach'
import _keys from 'lodash/keys'
import _reduce from 'lodash/reduce'
import _map from 'lodash/map'
import { TimerModels } from './timer.models'
import { map } from 'rxjs/operators'

export class TimersSession {
  public id: string
  protected forceDestroy$: Subject<any> = new Subject<any>()
  protected timers: { [key: string]: Timer } = {}

  constructor(protected destroyer$: Observable<any>) {
    this.id = uuidv4()
  }

  add(key: string, options?: TimerModels.TimerOptions) {
    this.timers[key] = new Timer(key, merge(this.destroyer$, this.forceDestroy$), options)
    return this.timers[key]
  }

  addMultiple(timersMap: { [key: string]: TimerModels.TimerOptions }) {
    return _map(timersMap, (options, key) => ({
      [key]: this.add(key, options)
    }))
  }

  timer(key: string) {
    return this.timers[key]
  }

  destroy() {
    this.forceDestroy$.next()
    this.timers = {}
  }

  init(keys?: string[]) {
    const k: any = keys || _keys(this.timers)
    _forEach(
      _filter(this.timers, (timer, key) => k.indexOf(key) > -1),
      (timer: Timer) => {
        timer.init()
      }
    )
  }

  reset(keys?: string[]) {
    const k: any = keys || _keys(this.timers)
    _forEach(
      _filter(this.timers, (timer, key) => k.indexOf(key) > -1),
      (timer: Timer) => {
        timer.reset()
      }
    )
  }

  start(keys?: string[]) {
    const k: any = keys || _keys(this.timers)
    _forEach(
      _filter(this.timers, (timer, key) => k.indexOf(key) > -1),
      (timer: Timer) => {
        timer.start()
      }
    )
  }

  stop(keys?: string[]) {
    const k: any = keys || _keys(this.timers)
    _forEach(
      _filter(this.timers, (timer, key) => k.indexOf(key) > -1),
      (timer: Timer) => {
        timer.stop()
      }
    )
  }

  pause(keys?: string[]) {
    const k: any = keys || _keys(this.timers)
    _forEach(
      _filter(this.timers, (timer, key) => k.indexOf(key) > -1),
      (timer: Timer) => {
        timer.pause()
      }
    )
  }

  values$(keys?: string[]): Observable<{ [key: string]: TimerModels.TimerValues }> {
    const k: any = keys || _keys(this.timers)
    const values: Array<BehaviorSubject<TimerModels.TimerValues>> = _reduce(
      _filter(this.timers, (timer, key) => k.indexOf(key) > -1),
      (timers: Array<BehaviorSubject<any>>, timer: Timer) => [...timers, timer.values$],
      []
    )
    return combineLatest(values).pipe(
      map((timerValues) => _reduce(timerValues, (vs, v, i) => ({ ...vs, [k[i]]: v }), {}))
    )
  }

  values(keys?: string[]): { [key: string]: TimerModels.TimerValues } {
    const k: any = keys || _keys(this.timers)
    return _reduce(
      _filter(this.timers, (timer, key) => k.indexOf(key) > -1),
      (timers, timer: Timer, i) => ({
        ...timers,
        [k[i]]: timer
      }),
      {}
    )
  }
}
