import { MongoRepository } from '@doesrobbiedream/nest-core'
import { Inject, Injectable } from '@nestjs/common'
import { Authentication } from '@doesrobbiedream/authentication-commons'
import { Model } from 'mongoose'
import { AuthenticationServer } from '../declarations/authentication-server.declarations'
import LocalUser = Authentication.LocalUser
import USER_TYPES = AuthenticationServer.USER_TYPES

@Injectable()
export class LocalUserRepository extends MongoRepository<LocalUser> {
  constructor(@Inject(USER_TYPES.LOCAL) entity: Model<LocalUser>) {
    super(entity)
  }
}
