import { ConfigModule, ConfigService } from '@nestjs/config'
import * as path from 'path'
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter'

export const MailerConfig = {
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: (config: ConfigService) => ({
    transport: {
      host: config.get<string>('MAILER_HOST'),
      port: config.get<number>('MAILER_PORT'),
      auth: {
        user: config.get<string>('MAILER_AUTH_USER'),
        pass: config.get<string>('MAILER_AUTH_PASS')
      }
    },
    defaults: {
      from: config.get<string>('MAILER_FROM_DEFAULT')
    },
    options: {
      partials: {
        dir: path.join(__dirname, config.get<string>('MAILER_TEMPLATES_FOLDER'), 'partials'),
        options: {
          strict: true
        }
      }
    },
    template: {
      dir: path.join(__dirname, config.get<string>('MAILER_TEMPLATES_FOLDER')),
      adapter: new HandlebarsAdapter(),
      options: {
        strict: true
      }
    }
  })
}
