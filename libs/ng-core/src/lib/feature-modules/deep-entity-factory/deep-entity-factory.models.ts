export namespace DeepEntityFactoryModels {
  export enum DEPENDENCY_MODE {
    EMBED = 'EMBED',
    RELATIONAL = 'RELATIONAL',
    COLLECTION = 'COLLECTION',
    EMBED_COLLECTION = 'EMBED_COLLECTION',
    ROOT = 'ROOT'
  }

  export type EntityMap = Record<string, Entity>
  export type DependenciesMap = Record<string, PropertyDefinition>

  export interface Entity {
    endpoint: string
    id_path?: string
  }

  export interface RootPropertyDefinition {
    entity: string
    dependencies?: DependenciesMap
    response_mapper?: (r: any) => any
    input_mapper?: (r: any) => any
  }

  export interface PropertyDefinition extends RootPropertyDefinition {
    named?: string
    mode: DEPENDENCY_MODE
  }

  export interface CarryOn {
    entities_map: EntityMap
    created: Record<string, Record<string, any>>
  }
}
