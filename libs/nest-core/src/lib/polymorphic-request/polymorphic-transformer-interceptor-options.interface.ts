export interface PolymorphicResponseInterceptorOptions {
  transformClasses: { [key: string]: new () => any }
  discriminator: string
}
