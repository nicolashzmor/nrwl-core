import { TestBed } from '@angular/core/testing'

import { DeepEntityFactoryService } from './deep-entity-factory.service'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { DeepEntityFactoryMocks } from './deep-entity-factory.mocks'
// import { HttpClient } from "@angular/common/http";
import TwoLevelsDeep = DeepEntityFactoryMocks.TwoLevelsDeep
import TwoLevelsDeepWithCollection = DeepEntityFactoryMocks.TwoLevelsDeepWithCollection
import ThreeLevelsDeepCollectionWithRepeatedInCollection = DeepEntityFactoryMocks.ThreeLevelsDeepCollectionWithRepeatedInCollection
import CreateMethodData = DeepEntityFactoryMocks.UnitTests.create

describe('DeepEntityFactoryService', () => {
  let service: DeepEntityFactoryService
  // let httpClient: HttpClient;
  let httpMock: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DeepEntityFactoryService]
    })
    service = TestBed.inject(DeepEntityFactoryService)
    httpMock = TestBed.get(HttpTestingController)
    // httpClient = TestBed.inject(HttpClient);
  })

  afterEach(() => {
    httpMock.verify()
  })

  // UNIT TESTS
  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('should create entity by calling http service', (done) => {
    service
      .create(CreateMethodData.input, CreateMethodData.property, CreateMethodData.carry_on())
      .subscribe((response) => {
        expect(response).toEqual('Homer')
        done()
      })
    const call = httpMock.expectOne('/user')
    expect(call.request.body).toEqual({ name: 'Homer' })
    call.flush('Homer')
  })

  it('should create & push a registry on the carry on', (done) => {
    const [i, p, c] = [
      CreateMethodData.input,
      CreateMethodData.property,
      CreateMethodData.carry_on()
    ]
    // ADD ID TO INPUT
    i.id = 'local_homer'
    service.create(i, p, c).subscribe((response) => {
      expect(response).toEqual('Homer')
      expect(c.created.user['local_homer']).toBeTruthy()
      done()
    })
    const call = httpMock.expectOne('/user')
    expect(call.request.body).toEqual({ id: 'local_homer', name: 'Homer' })
    call.flush('Homer')
  })

  it('should create & push a registry on the carry on with a mapped response', (done) => {
    const [i, p, c] = [
      CreateMethodData.input,
      CreateMethodData.property,
      CreateMethodData.carry_on()
    ]
    // ADD ID TO INPUT
    i.id = 'local_homer'
    p.response_mapper = (r) => r.name

    service.create(i, p, c).subscribe((response) => {
      expect(response).toEqual('Homer')
      expect(c.created.user['local_homer']).toEqual({
        id: 'remote_homer',
        name: 'Homer'
      })
      done()
    })

    const call = httpMock.expectOne('/user')
    expect(call.request.body).toEqual({ id: 'local_homer', name: 'Homer' })
    call.flush({ id: 'remote_homer', name: 'Homer' })

    p.response_mapper = (r) => r.id
    service.create(i, p, c).subscribe((response) => {
      expect(response).toEqual('remote_homer')
      done()
    })

    httpMock.expectNone('/user')
  })

  it('should create multiple entities (No nested)', (done) => {
    const [i, p, c] = [
      CreateMethodData.input,
      CreateMethodData.property,
      CreateMethodData.carry_on()
    ]

    i.id = 'local_homer'
    p.response_mapper = (r) => r.name

    const input = [i, i]

    service.runFactoryWithMultiple(input, c.entities_map, p).subscribe((response) => {
      expect(response).toEqual(['Homer', 'Homer'])
      done()
    })

    const call = httpMock.expectOne('/user')
    expect(call.request.body).toEqual({ id: 'local_homer', name: 'Homer' })
    call.flush({ id: 'remote_homer', name: 'Homer' })

    httpMock.expectNone('/user')
  })

  // INTEGRATION TESTS
  it('should create a two level entity', (done) => {
    service.runFactory(TwoLevelsDeep.Data, TwoLevelsDeep.Entities, TwoLevelsDeep.root).subscribe({
      next: (response) => expect(response).toEqual('remote_user_mock_1'),
      complete: () => done()
    })

    const addressCall = httpMock.expectOne('/address')
    addressCall.flush('remote_address_mock_1')

    const userCall = httpMock.expectOne('/user')
    expect(userCall.request.body).toEqual({
      name: 'Homer',
      address: 'remote_address_mock_1'
    })
    userCall.flush('remote_user_mock_1')
  })

  it('should create multiple two level entity with repeated items in collection', (done) => {
    let results: any[] = []
    service
      .runFactoryWithMultiple(
        TwoLevelsDeepWithCollection.Data,
        TwoLevelsDeepWithCollection.Entities,
        TwoLevelsDeepWithCollection.root
      )
      .subscribe({
        next: (response) => {
          results = response
        },
        complete: () => {
          expect(results).toEqual(['HOMER CREATED', 'MARGE CREATED'])
          done()
        }
      })
    // FIRST CALL > Homer
    // SHOULD CREATE CATEGORY
    const first_category_call = httpMock.expectOne('/category')
    expect(first_category_call.request.body).toEqual({
      _id: 'some_crazy_id',
      slug: 'category_1'
    })
    first_category_call.flush('remote_some_crazy_id')
    // SHOULD CREATE USER
    const first_user_call = httpMock.expectOne('/user')
    expect(first_user_call.request.body).toEqual({
      user: 'Homer',
      categories: ['remote_some_crazy_id']
    })
    first_user_call.flush('HOMER CREATED')

    // SECOND CALL > Marge
    // SHOULD NOT CREATE CATEGORY
    httpMock.expectNone('/category')
    // SHOULD CREATE USER
    const second_user_call = httpMock.expectOne('/user')
    expect(second_user_call.request.body).toEqual({
      user: 'Marge',
      categories: ['remote_some_crazy_id']
    })
    second_user_call.flush('MARGE CREATED')
  })

  it('should create three levels deep multiple entities without repetition', (done) => {
    let results: any[] = []
    service
      .runFactoryWithMultiple(
        ThreeLevelsDeepCollectionWithRepeatedInCollection.Data,
        ThreeLevelsDeepCollectionWithRepeatedInCollection.Entities,
        ThreeLevelsDeepCollectionWithRepeatedInCollection.root
      )
      .subscribe({
        next: (response) => {
          results = response
        },
        complete: () => {
          expect(results).toEqual(['Homer', 'Marge', 'Apu'])
          done()
        }
      })

    // First User /////////////////////////////////////////////////////////////////////////////////////
    //// Streets
    const streets_0$ = httpMock.match('/street')
    expect(streets_0$.length).toEqual(3)
    streets_0$.forEach((s) => s.flush(s.request.body['id']))
    //// Address
    const address_0$ = httpMock.expectOne('/address')
    expect(address_0$.request.body).toEqual({
      code: '00001',
      streets: [1, 2, 3]
    })
    address_0$.flush(address_0$.request.body.code)
    //// User
    httpMock.expectNone('/location') // IS EMBED
    const user_0$ = httpMock.expectOne('/user')
    expect(user_0$.request.body).toEqual({
      name: 'Homer',
      location: { address: '00001' }
    })
    user_0$.flush(user_0$.request.body.name)
    // Second User /////////////////////////////////////////////////////////////////////////////////////
    const streets_1$ = httpMock.expectOne('/street')
    streets_1$.flush(streets_1$.request.body['id'])
    //// Address
    const address_1$ = httpMock.expectOne('/address')
    expect(address_1$.request.body).toEqual({
      code: '00002',
      streets: [1, 2, 3, 4]
    })
    address_1$.flush(address_1$.request.body.code)
    //// User
    httpMock.expectNone('/location') // IS EMBED
    const user_1$ = httpMock.expectOne('/user')
    expect(user_1$.request.body).toEqual({
      name: 'Marge',
      location: { address: '00002' }
    })
    user_1$.flush(user_1$.request.body.name)
    // Third User /////////////////////////////////////////////////////////////////////////////////////
    httpMock.expectNone('/street')
    httpMock.expectNone('/address')
    httpMock.expectNone('/location')
    const user_2$ = httpMock.expectOne('/user')
    expect(user_2$.request.body).toEqual({
      name: 'Apu',
      location: { address: '00002' }
    })
    user_2$.flush(user_2$.request.body.name)
  })
})
