import { Test, TestingModule } from '@nestjs/testing'
import { OrchestratorServerService } from './orchestrator-server.service'
import { OrchestratorServerConfigs } from './orchestrator-server.configs'
import PROXY_PROVIDER = OrchestratorServerConfigs.PROXY_PROVIDER

describe('OrchestratorServerService', () => {
  let service: OrchestratorServerService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OrchestratorServerService, { provide: PROXY_PROVIDER, useValue: {} }]
    }).compile()

    service = module.get<OrchestratorServerService>(OrchestratorServerService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})
