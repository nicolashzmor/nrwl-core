import { Injectable } from '@nestjs/common'
import { ClientOptions, ClientProxy, ClientProxyFactory } from '@nestjs/microservices'

@Injectable()
export class MessageBrokerProxyService {
  constructor(protected config: ClientOptions) {
    this._client = this.proxyBuilder()
  }

  protected _client: ClientProxy

  public get client() {
    return this._client
  }

  protected proxyBuilder(): ClientProxy {
    return ClientProxyFactory.create(this.config)
  }
}
