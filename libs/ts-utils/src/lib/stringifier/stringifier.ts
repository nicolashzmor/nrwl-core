import _defaults from 'lodash/defaults'
import _has from 'lodash/has'

// TYPESCRIPT IMPLEMENTATION OF CryoJS
// https://github.com/hunterloftis/cryo/blob/master/lib/cryo.js
export class Stringifier {
  public static CONTAINER_TYPES = 'object array date function'.split(' ')

  public static REFERENCE_FLAG = '_STRINGIFIER_REF_'
  public static INFINITY_FLAG = '_STRINGIFIER_INFINITY_'
  public static FUNCTION_FLAG = '_STRINGIFIER_FUNCTION_'
  public static UNDEFINED_FLAG = '_STRINGIFIER_UNDEFINED_'
  public static DATE_FLAG = '_STRINGIFIER_DATE_'

  public static OBJECT_FLAG = '_STRINGIFIER_OBJECT_'
  public static ARRAY_FLAG = '_STRINGIFIER_ARRAY_'

  public stringify(item: Record<string, any>, options?: any): string {
    const references: any[] = []

    // Backward compatibility with 0.0.6 that exepects `options` to be a callback.
    options = typeof options === 'function' ? { prepare: options } : options
    options = _defaults(options || {}, {
      prepare: null,
      isSerializable: (item: any, key: string) => {
        return _has(item, key)
      }
    })

    const root = this.cloneWithReferences(item, references, options)

    return JSON.stringify({
      root: root,
      references: references
    })
  }

  public parse<T = Record<string, any>>(string: string, options?: any): T | null {
    const json = JSON.parse(string)

    options = typeof options === 'function' ? { finalize: options } : options
    options = _defaults(options || {}, { finalize: null })

    return this.rebuildFromReferences(json.root, json.references, options)
  }

  protected typeOf(item: any) {
    if (typeof item === 'object') {
      if (item === null) return 'null'
      if (item && item.nodeType === 1) return 'dom'
      if (item instanceof Array) return 'array'
      if (item instanceof Date) return 'date'
      return 'object'
    }
    return typeof item
  }

  protected rebuildFromReferences(item: any, references: any, options: any, restoredItems?: any) {
    restoredItems = restoredItems || []
    if (this.starts(item, Stringifier.REFERENCE_FLAG)) {
      const referenceIndex = parseInt(item.slice(Stringifier.REFERENCE_FLAG.length), 10)
      if (!_has(restoredItems, referenceIndex)) {
        const ref = references[referenceIndex]
        const container = this.unwrapConstructor(ref.value)
        const contents = ref.contents
        restoredItems[referenceIndex] = container
        Object.keys(contents).forEach(
          (k) =>
            (container[k] = this.rebuildFromReferences(
              contents[k],
              references,
              options,
              restoredItems
            ))
        )
      }

      // invoke callback after all operations related to serializing the item
      if (options.finalize) {
        options.finalize(restoredItems[referenceIndex])
      }

      return restoredItems[referenceIndex]
    }

    // invoke callback after all operations related to serializing the item
    if (options.finalize) {
      options.finalize(item)
    }

    return this.unwrap(item)
  }

  protected cloneWithReferences(item: any, references: any, options: any, savedItems?: any) {
    // invoke callback before any operations related to serializing the item
    if (options.prepare) {
      options.prepare(item)
    }

    savedItems = savedItems || []
    const type = this.typeOf(item)

    // can this object contain its own properties?
    if (Stringifier.CONTAINER_TYPES.indexOf(type) !== -1) {
      let referenceIndex = savedItems.indexOf(item)
      // do we need to store a new reference to this object?
      if (referenceIndex === -1) {
        const clone: any = {}
        referenceIndex =
          references.push({
            contents: clone,
            value: this.wrapConstructor(item)
          }) - 1
        savedItems[referenceIndex] = item
        Object.keys(item)
          .filter((k) => options.isSerializable(item, k))
          .forEach(
            (k) => (clone[k] = this.cloneWithReferences(item[k], references, options, savedItems))
          )
      }

      // return something like _CRYO_REF_22
      return Stringifier.REFERENCE_FLAG + referenceIndex
    }

    // return a non-container object
    return this.wrap(item)
  }

  protected wrap(item: any) {
    const type = this.typeOf(item)
    if (type === 'undefined') return Stringifier.UNDEFINED_FLAG
    if (type === 'function') return Stringifier.FUNCTION_FLAG + item.toString()
    if (type === 'date') return Stringifier.DATE_FLAG + item.getTime()
    if (item === Infinity) return Stringifier.INFINITY_FLAG
    if (type === 'dom') return undefined
    return item
  }

  protected wrapConstructor(item: any) {
    const type = this.typeOf(item)
    if (type === 'function' || type === 'date') return this.wrap(item)
    if (type === 'object') return Stringifier.OBJECT_FLAG
    if (type === 'array') return Stringifier.ARRAY_FLAG
    return item
  }

  protected unwrapConstructor(val: any) {
    if (this.typeOf(val) === 'string') {
      if (val === Stringifier.UNDEFINED_FLAG) return undefined
      if (this.starts(val, Stringifier.FUNCTION_FLAG)) {
        return this.unwrapFn(val)
      }
      if (this.starts(val, Stringifier.DATE_FLAG)) {
        const dateNum = parseInt(val.slice(Stringifier.DATE_FLAG.length), 10)
        return new Date(dateNum)
      }
      if (this.starts(val, Stringifier.OBJECT_FLAG)) {
        return {}
      }
      if (this.starts(val, Stringifier.ARRAY_FLAG)) {
        return []
      }
      if (val === Stringifier.INFINITY_FLAG) return Infinity
    }
    return val
  }

  protected unwrap(val: any) {
    if (this.typeOf(val) === 'string') {
      if (val === Stringifier.UNDEFINED_FLAG) return undefined

      if (this.starts(val, Stringifier.FUNCTION_FLAG)) {
        return this.unwrapFn(val)
      }
      if (this.starts(val, Stringifier.DATE_FLAG)) {
        const dateNum = parseInt(val.slice(Stringifier.DATE_FLAG.length), 10)
        return new Date(dateNum)
      }
      if (val === Stringifier.INFINITY_FLAG) return Infinity
    }
    return val
  }

  protected starts(string: string, prefix: string) {
    return this.typeOf(string) === 'string' && string.slice(0, prefix.length) === prefix
  }

  protected isNumber(n: any) {
    return !isNaN(parseFloat(n)) && isFinite(n)
  }

  protected unwrapFn(val: string) {
    const fn = val.slice(Stringifier.FUNCTION_FLAG.length)
    const argStart = fn.indexOf('(') + 1
    const argEnd = fn.indexOf(')', argStart)
    const args = fn.slice(argStart, argEnd)
    const arrowMarker = fn.indexOf('=>')
    const bodyStart = arrowMarker >= 0 ? arrowMarker + 2 : fn.indexOf('{') + 1
    const bodyEnd = arrowMarker >= 0 ? fn.length : fn.indexOf('}')
    let body = fn.slice(bodyStart, bodyEnd)
    // TEST IF ARROW FUNCTION DOESN'T HAVE A RETURN STATEMENT
    if (!RegExp(/^\s+{/).test(body)) {
      body = 'return ' + body
    }
    return new Function(args, body)
  }
}
