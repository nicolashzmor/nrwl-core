import { ExceptionHandlingDictionaries } from '../dictionaries'
import { ExceptionHandlingResponses } from '../responses'
import ErrorMessageDeclaration = ExceptionHandlingDictionaries.ErrorMessageDeclaration
import ExceptionResponse = ExceptionHandlingResponses.ExceptionResponse

export class Exception implements ExceptionResponse {
  status: number
  data: ExceptionHandlingResponses.ErrorData
  exception: string
  request_path: string
  timestamp: string

  constructor(error: ErrorMessageDeclaration, data?: any) {
    this.status = error.status
    this.data = { message: error.message, ...data }
    this.exception = error.exception
    this.request_path = ''
    this.timestamp = new Date().toISOString()
  }
}
